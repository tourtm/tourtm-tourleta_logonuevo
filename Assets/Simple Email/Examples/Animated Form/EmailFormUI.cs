﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EmailSender;
using RockVR.Video;
using EmailSender;


public class EmailFormUI : MonoBehaviour {

    public EmailMessage emailMsg;
    public InputField name;
    public InputField email;

    public GameObject Content;

	// Use this for initialization
	void Start () {
		
	}

    private void OnEnable()
    {
        // Resets fields
        ResetFormFields();
    }

    public void ShowContent()
    {
        if (!Content.activeSelf)
            Content.SetActive(true);
        // Resets form fields
        ResetFormFields();
    }

    public void HideContent()
    {
        if (Content.activeSelf)
            Content.SetActive(false);
        // Resets form fields
        ResetFormFields();
    }

    private void OnDisable()
    {
        // Resets fields
        ResetFormFields();
    }

    // Update is called once per frame
    void Update () {
		
	}

    private void ResetFormFields()
    {
        InputFieldReceiver emailInputReceiver = email.gameObject.GetComponent<InputFieldReceiver>();
        InputFieldReceiver.LastFocusedId = emailInputReceiver.GetInstanceID();
        email.text = "";
        emailInputReceiver.ClearText();
    }

    [ContextMenu("Show Player Files")]
    public void AddRecordedVideoToAttached()
    {
        // Gets the list of saved videos
        VideoPlayer.instance.SetRootFolder();
        Debug.LogFormat("<color=magenta>" + VideoPlayer.instance.videoFiles.Count + "</color>");
        // Gets the name of the last saved video
        string lastVideo = VideoPlayer.instance.videoFiles[VideoPlayer.instance.videoFiles.Count - 1];
        Debug.LogFormat("<color=magenta>" + lastVideo + "</color>");
        // Add the last recorded video to the attached files list, then the video it is uploaded.
        Debug.Log(lastVideo);
        // NOTE: This project is highly coupled
        emailMsg.AddAttachedFile(lastVideo);
    }
}
