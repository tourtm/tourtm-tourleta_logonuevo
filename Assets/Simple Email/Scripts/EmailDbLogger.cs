﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EmailSender
{

    public class EmailDbLogger : MonoBehaviour
    {

        EmailsDB emailsDB = null;

        // Use this for initialization
        void Start()
        {
            emailsDB = new EmailsDB("emails_log1", "Emails2.sqlite");
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void OnSendingStart(EmailInfo email)
        {

        }

        public void AddEmailAsSent(EmailInfo email)
        {
            email.status = EmailStatus.Sent;
            Debug.Log("Sent Successfully");
            // Debug.Log(email.ToString());
            // Save in database
            // If email id == -1 then creates a new row in the database
            emailsDB.UpdateEmail(email);
        }

        public void AddEmailAsNotSent(EmailInfo email)
        {
            email.status = EmailStatus.NotSentYet;
            Debug.Log("Not Sent :(");
            // Debug.Log(email.ToString());
            // If email id == -1 then creates a new row in the database
            emailsDB.UpdateEmail(email);
        }

    }

}
