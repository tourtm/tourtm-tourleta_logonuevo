﻿using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using UnityEngine;
using Firebase.Storage;
using System.Threading;
using System.Threading.Tasks;
using Dweiss;

// created by: j0se v1llalobos
// email template: https://mjml.io/try-it-live/BJ38OpOUP
namespace EmailSender
{

    interface IEmailEventHandler
    {
        /// <summary>
        /// Called as soon as the email start sending
        /// </summary>
        /// <param name="email"></param>
        void OnSendingStart(EmailInfo email);
        /// <summary>
        /// Called when the email gets successfully sent
        /// </summary>
        /// <param name="email"></param>
        void OnEmailSuccess(EmailInfo email);
        /// <summary>
        /// Called when the email fails to send
        /// </summary>
        /// <param name="email"></param>
        void OnEmailFail(EmailInfo email);
    }

    /// <summary>
    /// 
    /// </summary>
    [RequireComponent(typeof(EmailManager))]
    public class EmailMessage : MonoBehaviour, IEmailEventHandler
    {
        [HideInInspector]
        public EmailManager emailManager;
        public MergeTag videoLink;

        [HideInInspector]
        public RemainingEmails remainingEmails;

        public string from;

        [Header("Recipient")]
        [Tooltip("The email recipient")]
        public string to = null;
        public string subject = "How are you?";

        // End lines are marked with ascii-13 + ascii-10 (carriage return + new line)
        [Header("Message")]
        [Multiline(5)]        
        public string body = "Hello world, this is a demo message";
        public bool _isBodyHtml = false;

        [Header("Attachments")]
        public List<string> files = new List<string>();
        public List<string> files_download = new List<string>();

        [Header("Events")]
        public EmailEvent _OnSendingStart;
        public EmailEvent _OnEmailSuccess;
        public EmailEvent _OnEmailFail;


        // Minified in https://www.willpeavy.com/minifier/
        private readonly string plainEmailWrapperUp = "<!doctype html><html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\"><head> <title></title> <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"> <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"></head><body>";
        private readonly string plainEmailWrapperDown = "</body></html>";

        // Firebase classes
        FirebaseStorage storage = null;
        MetadataChange metadataMP4 = null;
        MailMessage msg = null;
        EmailInfo msgInfo = null;
        public System.Action<EmailInfo> uploadSuccess = null;

        #region UnityMethods
        private void Awake()
        {
            emailManager = GetComponent<EmailManager>();
        }

        public void Start()
        {
            from = Settings.Instance.EmailMessageFrom;
            subject = Settings.Instance.EmailMessageSubject;
        }

        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.T))
            {
                GetFiles();
            }
        }
        #endregion


        // Keep in mind that un-sent emails are not persistent until the Send method gets called
        [ContextMenu("Send")]
        public void Send()
        {
            // The mail creation is an async process because it is required to upload the attached video 
            CreateMailMsgFromFields((msg) => {
                Debug.Log("Email Created");
                if (msg == null) return; // Checks the message is sent
                if (msg.attachments_download == null || msg.attachments_download.Count == 0) return; // Checks the video were uploaded

                Debug.Log("Sending The Message Baby");
                EmailInfo msgEdited = msg;
                // Debug.Log(MergeTagsBuilder(msg.body));
                // Debug.LogFormat("<color=green>" + msgEdited.body + "</color>");

                // Sets the value to the #userVideoLink# tag
                int lastVideo = msg.attachments_download.Count - 1;
                videoLink.customText = msg.attachments_download[lastVideo];

                // adds download link to button
                // When #userVideoLink# is replaced before the video gets uploaded
                // the tag is set to "", then when we look for this tag in tis point of code
                // the #userVideoLink# does not exists and therefore the link value is not set.
                // It was solved using the tag name as value by default
                // msgEdited.body = MergeTagsBuilder(msg.body); // #userVideoLink# has a value
                msgEdited.body = MergeTagsBuilder(body);
                Debug.Log("POINT REACHED");
                Debug.Log(msgEdited.body);

                SimpleEmailSender.SendAsync(msgEdited, OnEmailSuccess, OnEmailFail, (EmailInfo) => {
                    Debug.Log("Sending Email deleg");
                });
            });            
        }



        // Attached files helpers methods
        string lastAttachedFile = null;
        /// <summary>
        /// Adds the file at filePath to the attachments
        /// </summary>
        /// <param name="filePath">The full path of the file to attach</param>
        public void AddAttachedFile(string filePath, bool clearList = true)
        {
            if (files == null)
            {
                // Initializes files field
                files = new List<string>();
            }

            // Removes all the videos to ensure a new gameplay session
            // will show its video and no the video of the first session.
            if (clearList)
            {
                files.Clear();
            }

            lastAttachedFile = filePath;
            files.Add(lastAttachedFile);
        }

        /// <summary>
        /// Removes the last attached file
        /// </summary>
        public void RemoveLastAttachedFile()
        {
            // If has not been attached a file then do nothing
            if (lastAttachedFile == null) return;

            if (files == null || files.Count == 0) return; // Nothing to remove here

            files.Remove(lastAttachedFile);
        }

        /// <summary>
        /// Removes all the attached files
        /// </summary>
        public void RemoveAllAttachedFiles()
        {
            if (files == null || files.Count == 0) return; // Nothing to clear here

            // Remove all files
            files.Clear();
        }

        // TODO: Implement merge tag support in TO and SUBJECT fields

        /// <summary>
        /// Replaces the merge tags in the message body with the values supplied on the MergeTags behaviours
        /// </summary>
        /// <param name="message">The raw message</param>
        /// <returns>The message with merged tags values</returns>
        public string MergeTagsBuilder(string message)
        {
            // All the merge tags on this scene
            MergeTag[] mergeTags = FindObjectsOfType<MergeTag>();
            // cleans special chars
            message = message.Trim();
            
            // Iterates each merge tag
            foreach (MergeTag mergeTag in mergeTags)
            {
                // create full tag removing whitespaces from tag name
                string tagFullName = "#" + mergeTag.Name.Replace(" ", string.Empty) + "#";
                // Assign to message the value for the current tag 
                message = message.Replace(tagFullName, mergeTag.Value);
            }

            return message;
        }

        [System.Serializable]
        public struct charInfo
        {
            public char character;
            public int asciiCode;
        }

        List<string>.Enumerator filesEnumerator;
        public void Recursive()
        {
            if (filesEnumerator.MoveNext())
            {
                Debug.Log(filesEnumerator.Current);
                Recursive();
            }
        }
        public void GetFiles()
        {
            filesEnumerator = files.GetEnumerator();
            Recursive();
        }

        public void UploadRecursive(System.Action<EmailInfo> onSuccess)
        {
            // upload all the attached files on this message
            if (filesEnumerator.MoveNext()) // If a file is available
            {
                if (filesEnumerator.Current != null && !filesEnumerator.Current.Equals(""))
                    if (File.Exists(filesEnumerator.Current))
                    {
                        // Gets file name
                        // splits the file path using \ or / to get file name
                        string[] fileParts = filesEnumerator.Current.Split('\\', '/');
                        string fileName = fileParts[fileParts.Length - 1];

                        /*
                        * Crea una referencia para subir, descargar o borrar un archivo, o
                        * para obtener o actualizar sus metadatos. Se puede decir que una
                        * referencia es un indicador que apunta a un archivo en la nube.
                        */
                        // It seems like the firebase path don't required auth info, then anyone with
                        // this reference URL could upload/download videos by using the API
                        // Note: If you want to set some auth rules, go to firebase 'use rules' and do some magic.
                        // Note2: An app recompilation and config file update will be required in order to suuport it in future releases.
                        StorageReference reference = storage.GetReferenceFromUrl(Settings.Instance.FirebaseUploadPath + fileName);

                        // Upload all the attached files
                        reference.PutFileAsync(filesEnumerator.Current, metadataMP4, null, CancellationToken.None, null)
                            .ContinueWith((Task<StorageMetadata> task) => {
                                if (task.IsFaulted || task.IsCanceled)
                                {
                                    Debug.LogError(task.Exception.ToString());
                                    // Uh-oh, an error occurred!
                                }
                                else
                                {
                                    // Gets download url
                                    Task<System.Uri> link_async = reference.GetDownloadUrlAsync();
                                    link_async.ContinueWith((Task<System.Uri> linktask) =>
                                    {
                                        string download_url = linktask.Result.ToString();
                                        // Removes previously uploaded videos to ensure the video recorded in current
                                        // gameplay session is shown.
                                        // TODO: Check for pending email resend
                                        files_download.Clear();
                                        files_download.Add(download_url);
                                        UploadRecursive(onSuccess);
                                    }, TaskScheduler.FromCurrentSynchronizationContext());
                                }
                            }, TaskScheduler.FromCurrentSynchronizationContext());
                        // TaskScheduler.FromCurrentSynchronizationContext()
                        // is used to specify that the task continuation should execute on the main thread
                        // https://stackoverflow.com/questions/4331262/task-continuation-on-ui-thread
                        // This is required to execute sqlite commands (Must be executed on main thread)
                    }
            }
            else
            {
                msg.IsBodyHtml = true;
                // Commented because when code reaches this point, the mergetag component is disabled, 
                // then, the tag does not exists.
                // Sets "VER MAS" link href.

                // at this point #userVideoLink# has a value
                msg.Body = MergeTagsBuilder(msg.Body);
                Debug.Log(msg.Body);

                msgInfo = new EmailInfo();
                msgInfo.Message = msg;

                msgInfo.attachments = files;
                Debug.Log("FilesDownloads " + files_download.Count);
                msgInfo.attachments_download = new List<string>();                
                foreach (string file in files_download)
                {
                    msgInfo.attachments_download.Add(file);
                }

                // Sends the callback response
                onSuccess.Invoke(msgInfo);
            }
        }

        public void CreateEmailFromEmailInfo(EmailInfo email, System.Action<EmailInfo> onSuccess)
        {
            // Gets a reference to the storage instance
            storage = FirebaseStorage.DefaultInstance;
            metadataMP4 = new MetadataChange();
            metadataMP4.ContentType = "video/mp4"; // Allows to play uploaded video in internet
            // resets the list of links
            // files_download = new List<string>();

            if ((email.attachments != null && email.attachments.Count > 0) &&
                (email.attachments_download == null || email.attachments_download.Count == 0)) // checks there are attached files
            {
                files = email.attachments;
                filesEnumerator = files.GetEnumerator();

                msg = email.Message;
                UploadRecursive(onSuccess);
            }
        }

        public void CreateMailMsgFromFields(System.Action<EmailInfo> onSuccess)
        {
            // Gets a reference to the storage instance
            storage = FirebaseStorage.DefaultInstance;
            metadataMP4 = new MetadataChange();
            metadataMP4.ContentType = "video/mp4"; // Allows to play uploaded video in internet

            // First, upload attached files because it is an async operation:
            if (files != null && files.Count > 0) // checks there are attached files
            {

                // Mail message
                msg = new MailMessage(MergeTagsBuilder(from), MergeTagsBuilder(to));
                msg.IsBodyHtml = true;
                msg.Subject = MergeTagsBuilder(subject);
                msg.Body = MergeTagsBuilder(body);
                Debug.Log(msg.Body);

                filesEnumerator = files.GetEnumerator();
                UploadRecursive(onSuccess);
            }
            else
            {
                EmailInfo theMsg = BuildMsgWithNoAttachedFiles(); // No attached videos, then build the email normally
                onSuccess(theMsg); // Sends the message to the callback
            }
        }

        public EmailInfo BuildMsgWithNoAttachedFiles()
        {
            if (emailManager.UserName == null)
            {
                // TODO: Show friendly message in inspector telling to user that please supply a user email
                // TODO: Make re-gex email validation.
                return null;
            }
            // Mail message
            string from = "tour@telemedellin.tv";
            Debug.Log(emailManager.UserName);
            Debug.Log(from);
            MailMessage msg = new MailMessage(from, MergeTagsBuilder(to));
            if (subject == null || subject == "")
            {
                // TODO: Show to user in inspector that should supply a valid subject
                return null;
            }
            msg.Subject = MergeTagsBuilder(subject);

            if (body == null || body == "")
            {
                // TODO: Show to user in inspector that should supply a valid body
                return null;
            }
            // set always to true for avoid email servers as gmails changing some lines to purple or other undesired effects.
            msg.IsBodyHtml = true;
            // unity input field line ending is cr-lf (\r\n) https://goo.gl/LxrHXX

            if (_isBodyHtml) // html
            {
                // Replace merge tags
                msg.Body = MergeTagsBuilder(body);
            }
            else // Plain text
            {
                // Message is wrapped in a <pre> element for avoid undesired effects as purple lines on web clients as gmail.
                // The <pre> tag defines preformatted text.
                // Text in a <pre> element is displayed in a fixed-width font(usually Courier), and
                // it preserves both spaces and line breaks.
                msg.Body = "<pre>" + MergeTagsBuilder(body) + "</pre>";
            }

            EmailInfo msgInfo = new EmailInfo();
            msgInfo.Message = msg;

            return msgInfo;
        } 

        public void OnSendingStart(EmailInfo email)
        {
            _OnSendingStart.Invoke(email);
        }

        public void OnEmailSuccess(EmailInfo email)
        {
            _OnEmailSuccess.Invoke(email);
        }

        public void OnEmailFail(EmailInfo email)
        {
            _OnEmailFail.Invoke(email);            
        }

    }
}

