﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace EmailSender
{
    /// <summary>
    /// The different possible inputs for create merge tags
    /// </summary>
    public enum MergeTagInput
    {
        InputField,
        DropdownText,
        Slider,
        Toggle,
        CustomValue
    }

    /// <summary>
    /// Behaviour used for create a new MergeTag for the merge tag system.
    /// </summary>
    public class MergeTag : MonoBehaviour
    {
        /// <summary>
        /// The name of this merge tag, remember that two different MergeTags should
        /// not have the same name
        /// </summary>
        public string Name = null;

        /// <summary>
        /// Input source from which will be assigned the Value property.
        /// </summary>
        public MergeTagInput ValueFrom = MergeTagInput.InputField;
        public bool uppercase = false;

        // The values of the following fields are changed from custom inspector code
        [HideInInspector]
        public string toggleTxtOn = "Yes";
        [HideInInspector]
        public string toggleTxtOff = "No";
        [HideInInspector]
        public string customText = "I am a custom text";

        // TODO: Implement an editor window to manage all the merge tags
        // TODO: Proposal, Implement a merge tag recommendation system on the message composition window

        public string Value
        {
            get
            {
                string val = null;
                switch (ValueFrom)
                {
                    case MergeTagInput.InputField:
                        string txt = GetComponent<InputField>().text;
                        val = uppercase ? txt.ToUpper() : txt;
                        break;

                    case MergeTagInput.DropdownText:
                        Dropdown drop = GetComponent<Dropdown>();
                        // Selects the actual value text in the dropdown
                        val = drop.options[drop.value].text;
                        break;

                    case MergeTagInput.Slider:
                        // Gets the current slider value as a string
                        val = GetComponent<Slider>().value.ToString();
                        break;

                    case MergeTagInput.Toggle:
                        val = (GetComponent<Toggle>().isOn) ? toggleTxtOn : toggleTxtOff;
                        break;

                    case MergeTagInput.CustomValue:
                        val = customText;
                        break;

                }

                return val;
            }
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}