﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


namespace EmailSender
{
    [CustomEditor(typeof(EmailManager))]
    public class EmailManagerEditor : Editor
    {
        SerializedProperty userPass;
        private void OnEnable()
        {
            userPass = serializedObject.FindProperty("UserPassword");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            base.OnInspectorGUI();

            GUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("User Password");
            userPass.stringValue = EditorGUILayout.PasswordField(userPass.stringValue);
            GUILayout.EndHorizontal();

            serializedObject.ApplyModifiedProperties();
        }

    }
}

