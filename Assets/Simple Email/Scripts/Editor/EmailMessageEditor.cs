﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace EmailSender
{
    [CustomEditor(typeof(EmailMessage))]
    public class EmailMessageEditor : Editor
    {

        public void OnEnable()
        {
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            base.OnInspectorGUI();

            
            EmailMessage mailMsg = (EmailMessage) target;

            GUILayout.Space(10);
            if (GUILayout.Button("Send"))
            {
                mailMsg.Send(); // Send the email
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}

