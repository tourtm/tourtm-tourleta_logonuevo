﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace EmailSender
{
    [CustomEditor(typeof(MergeTag))]
    public class MergeTagEditor : Editor
    {

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            base.OnInspectorGUI();

            MergeTag mergeTag = target as MergeTag;
            if (mergeTag == null) return; // Checks that merge tag is instantiated
            
            
            // Show toggle text fields for the on/off state
            if (mergeTag.ValueFrom == MergeTagInput.Toggle)
            {
                SerializedProperty toggleTxtOn = serializedObject.FindProperty("toggleTxtOn");
                SerializedProperty toggleTxtOff = serializedObject.FindProperty("toggleTxtOff");

                GUILayout.BeginHorizontal();
                // The text to show when toggle get On state
                EditorGUILayout.PrefixLabel("Text Toggle On");
                toggleTxtOn.stringValue = EditorGUILayout.TextField(toggleTxtOn.stringValue);
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                // The text to show when toggle get Off state
                EditorGUILayout.PrefixLabel("Text Toggle Off");
                toggleTxtOff.stringValue = EditorGUILayout.TextField(toggleTxtOff.stringValue);
                GUILayout.EndHorizontal();
            }
            else if (mergeTag.ValueFrom == MergeTagInput.CustomValue) // If value from field is set to CustomValue...
            {
                // Show the custom value text field

                // are used serialized properties for ensure out of the box undo/redo and prefab compatibility
                SerializedProperty customText = serializedObject.FindProperty("customText");
                GUILayout.BeginHorizontal();
                EditorGUILayout.PrefixLabel("Custom Text");                
                customText.stringValue = EditorGUILayout.TextField(customText.stringValue);
                GUILayout.EndHorizontal();
            }

            serializedObject.ApplyModifiedProperties();
        }

    }
}

