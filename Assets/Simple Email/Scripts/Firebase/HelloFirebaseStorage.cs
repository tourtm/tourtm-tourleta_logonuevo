﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Storage;
using System.Threading;
using System.Threading.Tasks;


public class HelloFirebaseStorage : MonoBehaviour {

    public string videoFolder = null;
    // The video path at disk
    public string fileName = null;

	// Use this for initialization
	void Start () {
       

	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("Uploading File");
            UploadVideoMP4();
        }
	}


    public void UploadVideoMP4()
    {
        // Gets a reference to the storage instance
        FirebaseStorage storage = FirebaseStorage.DefaultInstance;
        MetadataChange metadata = new MetadataChange();
        metadata.ContentType = "video/mp4"; // Allows to play uploaded video in internet

        /*
         * Crea una referencia para subir, descargar o borrar un archivo, o
         * para obtener o actualizar sus metadatos. Se puede decir que una
         * referencia es un indicador que apunta a un archivo en la nube.
         */
        StorageReference reference = storage.GetReferenceFromUrl("gs://tourleta-b1f4a.appspot.com/tourleta/myfile.mp4");

        reference.PutFileAsync(videoFolder + fileName, metadata, null, CancellationToken.None, null)
            .ContinueWith((Task<StorageMetadata> task) => {
                if (task.IsFaulted || task.IsCanceled)
                {
                    Debug.Log(task.Exception.ToString());
                    // Uh-oh, an error occurred!
                }
                else
                {
                    Task<System.Uri> link_async = reference.GetDownloadUrlAsync();
                    // ContinueWith() Creates a continuation that executes asynchronously when the target Task completes.
                    link_async.ContinueWith((Task<System.Uri> linktask) =>
                    {
                        string download_url = linktask.Result.ToString();
                        Debug.Log("Finished uploading...");
                        Debug.Log("download url = " + download_url);
                    });
                }
            });
    }

}
