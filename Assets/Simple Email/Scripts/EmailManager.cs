﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dweiss;

namespace EmailSender
{
    /// <summary>
    /// Holds the auth and server info.
    /// </summary>
    [RequireComponent(typeof(EmailMessage))]
    public class EmailManager : MonoBehaviour
    {

        public enum EmailServer
        {
            Gmail,
            Custom
        }

        // [HideInInspector]
        public RemainingEmails remainingEmails;
        
        [Header("Options")]
        [Tooltip("Send emails that has not been sent yet")]
        public bool sendPendingEmailsOnStart = true;

        [Header("Server info")]
        public EmailServer emailServer = EmailServer.Gmail;
        public string SMTPServer;
        public int SMTPPort;

        [Header("Authentication info")]
        [Tooltip("The email address you are going to use for send emails")]
        public string UserName;
        [Tooltip("The password of your email address")]
        [HideInInspector]
        public string UserPassword;

        public void Start()
        {
            SMTPServer = Settings.Instance.SMTPServer;
            SMTPPort = Settings.Instance.SMTPPort;
            UserName = Settings.Instance.SMTPUserName;
            UserPassword = Settings.Instance.SMTPUserPassword;

            SimpleEmailSender.emailAuthInfo = new SimpleEmailSender.EmailAuth
            {
                SMTPServer = this.SMTPServer,
                SMTPPort = this.SMTPPort,
                UserName = this.UserName,
                UserPass = this.UserPassword
            };
            // SimpleEmailSender.Auth(SimpleEmailSender.emailAuthInfo);
            // TODO: Expose OnAuth event that gets called when user connects to a smtp server
            //if (sendPendingEmailsOnStart)
            //{
            //    StartCoroutine(remainingEmails.TryToSendPendingEmails());
            //}            
        }

        public void OnValidate()
        {
            if (emailServer == EmailServer.Gmail)
            {
                SMTPServer = "smtp.gmail.com";
                SMTPPort = 587;
            }
        }

    }

}
