﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogStatus : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnSendingStart()
    {
        Debug.Log("Email Sending");
    }

    public void OnSent()
    {
        Debug.Log("Email Sent");
    }

    public void OnError()
    {
        Debug.Log("Email Error");
    }
}
