﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Storage;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace EmailSender
{
    /// <summary>
    /// This helper class re sends all the emails with not-sent-yet status
    /// </summary>
    public class EmailResender : MonoBehaviour
    {

        EmailsDB emailsDB = null;

        public EmailMessage msgWriter;

        public List<EmailInfo> notSentEmails;
        public List<EmailInfo>.Enumerator notSentEmailsEnumerator;

        public List<string> downloads;

        // Use this for initialization
        void Start()
        {
            emailsDB = new EmailsDB("emails_log1", "Emails2.sqlite");
        }

        // Update is called once per frame
        void Update()
        {

        }

        /// <summary>
        /// Starts resending emails that could no be send
        /// </summary>
        [ContextMenu("Resend Failed")]
        public void StartFailedResend()
        {
            notSentEmails = emailsDB.GetNotSendedEmails();
            // Gets the enumerator
            notSentEmailsEnumerator = notSentEmails.GetEnumerator();
            // Send all the emails
            SendRecursive();
        }

        public void SendRecursive()
        {
            // Current position is in -1
            if (notSentEmailsEnumerator.MoveNext()) // now, Current position is in 0
            {
                // Attached files were already uploaded to the cloud
                if (notSentEmailsEnumerator.Current.attachments_download != null || notSentEmailsEnumerator.Current.attachments_download.Count > 0)
                {
                    Debug.Log(string.Format("Sending Email (rowid={0})", notSentEmailsEnumerator.Current.rowid));
                    // Send email
                    SimpleEmailSender.SendAsync(notSentEmailsEnumerator.Current, (sentEmail) => {
                        sentEmail.status = EmailStatus.Sent; // changes email state to sent
                        emailsDB.UpdateEmail(sentEmail); // update email in database
                    });
                }
                else // Attached files has not been uploaded yet
                {
                    Debug.Log(string.Format("Uploading attached files and Sending Email (rowid={0})", notSentEmailsEnumerator.Current.rowid));
                    // Upload attached files
                    msgWriter.CreateEmailFromEmailInfo(notSentEmailsEnumerator.Current, (msg) =>
                    {
                        Debug.Log("Email created");
                        // Updates modified, fields
                        notSentEmailsEnumerator.Current.body = msg.body;
                        // Adds attached videos
                        notSentEmailsEnumerator.Current.attachments_download = msg.attachments_download;
                        SimpleEmailSender.SendAsync(notSentEmailsEnumerator.Current, (sentEmail) =>
                        {
                            sentEmail.status = EmailStatus.Sent; // changes email state to sent
                            emailsDB.UpdateEmail(sentEmail); // update email in database
                            Debug.Log(string.Format("Email sent successfully (rowid={0})", sentEmail.rowid));
                            SendRecursive(); // Send next not-sent-yet email                        
                        });
                    });
                }
            }
            else // no more emails to send
            {
                Debug.Log("No More Emails To Send");
            }
        }

    }

}
