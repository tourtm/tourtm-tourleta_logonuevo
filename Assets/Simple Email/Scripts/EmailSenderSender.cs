﻿using System;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

namespace EmailSender
{
    // Remember to set Api Compatibility Level to ".NET 2.0" instead of ".NET 2.0 Subset" under player settings.
    // If you dont do it, then when the game gets compiled System.Net.Mail namespace is not going to be included https://goo.gl/YAEiGV

    /// <summary>
    /// This is a class with static members for send emails using SmtpClient class.
    /// </summary>
    public class SimpleEmailSender
    {
        // Email authentication information
        public static EmailAuth emailAuthInfo = new EmailAuth { SMTPServer = "", SMTPPort = 0, UserName = "", UserPass = "" };
        public struct EmailAuth
        {
            public string SMTPServer;
            public int SMTPPort;
            public string UserName;
            public string UserPass;
        }

        public static SmtpClient client = null;
        public static SmtpClient Auth(EmailAuth authInfo)
        {
            try
            {
                // Debug.Log("UserName: " + emailAuthInfo.UserName + ", Password: " + emailAuthInfo.UserPass);
                // SMTP authentication
                // Connects this SmtpClient with a SmtpServer
                client = new SmtpClient(authInfo.SMTPServer, authInfo.SMTPPort);
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.EnableSsl = true;
                client.Credentials = new NetworkCredential(authInfo.UserName, authInfo.UserPass) as ICredentialsByHost;
                ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                {
                    return true;
                };

                return client;
            }
            catch (Exception ex)
            {
                Debug.LogWarning("EmailSender: " + ex);
                throw ex;
            }
        }
        
        /// <summary>
        /// Synchronous sender
        /// </summary>
        /// <param name="mailMessage"></param>
        public static void Send(EmailInfo mailMessage)
        {
            try
            {
                // SMTP authentication
                // Connects this SmtpClient with a SmtpServer
                //if (mailServer == null)
                client = Auth(emailAuthInfo);
                
                Debug.Log("EmailSender: Sending Email (Synchronous).");
                // Send the email.
                client.Send(mailMessage.Message);
                Debug.Log("EmailSender: Successfully sent Email (Synchronous).");
            }
            catch (Exception ex)
            {
                Debug.LogWarning("EmailSender: " + ex);
            }
        }

        public static void SendAsync(EmailInfo mailMessage, Action<EmailInfo> onSucces=null, Action<EmailInfo> onFail = null, Action<EmailInfo> sendingStart=null)
        {
            try
            {
                // SMTP authentication
                // Connects this SmtpClient with a SmtpServer
                // if (client == null)

                // Creating an SmtpClient instance each time this method is invoked
                // to ensure no SendAsync exception in progress
                client = Auth(emailAuthInfo);

                // Register for receive server events.
                client.SendCompleted += new SendCompletedEventHandler(delegate (object o, AsyncCompletedEventArgs ev) {
                    if (!ev.Cancelled && ev.Error == null)
                    {
                        if (onSucces != null)
                        {
                            onSucces(mailMessage);
                        }
                    }
                    else
                    {
                        if (onFail != null)
                        {
                            onFail(mailMessage); // Invokes the on fail callback                            
                        }
                        Debug.Log(ev.Error.ToString());
                    }
                });

                // Send the email.
                client.SendAsync(mailMessage.Message, "");

                // Calls the sendingStart delegate
                if (sendingStart != null) sendingStart(mailMessage);

                Debug.Log("EmailSender: Sending Email.");
            }
            catch (Exception ex)
            {
                Debug.LogError("EmailSender: " + ex);
                if (onFail != null) onFail(mailMessage); // Invokes the on fail callback
            }
        }

        public static void Send(string to, string subject, string body, string attachFile, Action<object, AsyncCompletedEventArgs> callback)
        {
            try
            {
                // SMTP authentication
                // Connects this SmtpClient with a SmtpServer
                SmtpClient mailServer = Auth(emailAuthInfo);

                // Mail message
                MailMessage msg = new MailMessage(emailAuthInfo.UserName, to);
                msg.Subject = subject;
                msg.Body = body;
                if (attachFile != null && !attachFile.Equals(""))
                    if (File.Exists(attachFile))
                        msg.Attachments.Add(new Attachment(attachFile));

                // Register for receive server events.
                mailServer.SendCompleted += new SendCompletedEventHandler(callback);

                // Send the email.
                mailServer.SendAsync(msg, "");

                Debug.Log("EmailSender: Sending Email.");
            }
            catch (Exception ex)
            {
                Debug.LogWarning("EmailSender: " + ex);
                callback("", new AsyncCompletedEventArgs(ex, true, ""));
            }
        }
    }
}