﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data;
using Mono.Data.Sqlite;
using System.IO;

public class SQLiteCrudBase : System.IDisposable {

    public readonly string dbConnectionString;
    public readonly IDbConnection dbConnection;
    
    // Constructor
    public SQLiteCrudBase(string dbName)
    {
        // Creates databases at (windows): 
        // C:\Users\EQUIPO\AppData\LocalLow\Telemedellin\EmailSenderMain
        // If you want to know were are your database file just log it:
        // Debug.Log(Application.persistentDataPath);
        dbConnectionString = "URI=file:" + Application.persistentDataPath + "/" + dbName;
        Debug.Log("Connecting to: " + dbConnectionString);
        dbConnection = new SqliteConnection(dbConnectionString);
        dbConnection.Open();
        Debug.Log("Connected to: " + dbConnectionString);
    }

    // Destructor
    ~SQLiteCrudBase()
    {
        dbConnection.Close();
    }

    // HELPER METHODS
    public IDbCommand GetDBCommand()
    {
        return dbConnection.CreateCommand();
    }

    // READ
    public IDataReader GetAllData(string tableName)
    {
        IDbCommand cmnd = dbConnection.CreateCommand();
        cmnd.CommandText = "SELECT rowid, * FROM " + tableName;
        return cmnd.ExecuteReader();
    }
    public virtual IDataReader GetRowById(string tableName,  int rowid)
    {
        IDbCommand cmnd = dbConnection.CreateCommand();
        cmnd.CommandText = "SELECT rowid, * FROM " + tableName + " WHERE rowid = " + rowid.ToString();
        return cmnd.ExecuteReader();
    }
    public virtual IDataReader[] GetRowsById(string tableName, params int[] rowid)
    {
        // An array for store each rowid specified in the params list
        IDataReader[] readers = new IDataReader[rowid.Length];
        for (int i = 0; i < rowid.Length; i++)
        {
            readers[i] = GetRowById(tableName, rowid[i]);
        }

        return readers;
    }
    public int GetNumOfRows(string tableName)
    {
        IDbCommand cmnd = dbConnection.CreateCommand();
        cmnd.CommandText = "SELECT COUNT(*) FROM " + tableName;
        IDataReader reader = cmnd.ExecuteReader();
        return (int) reader[0]; // Un-boxing operation done here
    }
    
    // DELETE
    public void DeleteAllData(string tableName)
    {
        IDbCommand cmnd = dbConnection.CreateCommand();
        cmnd.CommandText = "DROP IF TABLE EXISTS " + tableName;
        cmnd.ExecuteNonQuery();
    }
    /// <summary>
    /// The row's rowid to delete
    /// </summary>
    /// <param name="rowid">The rowid</param>
    public virtual void DeleteRowByID(int rowid)
    {
        IDbCommand cmnd = dbConnection.CreateCommand();
        cmnd.CommandText = "DELETE FROM MyTable WHERE rowid = " + rowid.ToString();
        cmnd.ExecuteNonQuery();
    }
    /// <summary>
    /// Deletes the specified rows
    /// </summary>
    /// <param name="rowid">The list of rows to delete</param>
    public virtual void DeleteRowsByID(params int[] rowid)
    {
        foreach (int row in rowid)
        {
            DeleteRowByID(row);
        }
    }

    public virtual void Close()
    {
        dbConnection.Close();
        Debug.Log("Connection to " + dbConnectionString + " successfully closed.");
    }

    public virtual void Dispose()
    {
        dbConnection.Close();
        Debug.Log("Connection to " + dbConnectionString + " successfully closed.");
    }
}
