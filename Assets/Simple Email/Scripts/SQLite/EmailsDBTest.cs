﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EmailSender;

public class EmailsDBTest : MonoBehaviour {
    [Header("Database Info")]
    public string tableName = null;
    public string dbName = null;

    [Header("Result")]
    public List<EmailInfo> emailsInDB = null;

    [Header("Actual Email")]
    public EmailInfo email;
    public EmailsDB emailsDB = null;
    

	// Use this for initialization
	void Start () {
        emailsDB = new EmailsDB(tableName, dbName);
        Debug.Log(Application.persistentDataPath);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    [ContextMenu("Add Email")]
    public void AddEmail()
    {
        emailsDB.AddData(email);
    }

    [ContextMenu("Update Email")]
    public void UpdateEmail()
    {
        emailsDB.UpdateEmail(email);
    }

    [ContextMenu("Get All Not Sended Emails")]
    public void GetAllNotSentEmails()
    {
        emailsInDB = emailsDB.GetNotSendedEmails();
    }

    [ContextMenu("Get All Emails")]
    public void GetAllEmails()
    {
        emailsInDB = emailsDB.GetAllEmails();
    }
}
