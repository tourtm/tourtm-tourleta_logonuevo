﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data;
using Mono.Data.Sqlite;
using System.IO;
using EmailSender;


public class EmailsDB : SQLiteCrudBase {

    /// <summary>
    /// The database columns
    /// </summary>
    public enum EmailsDBColumns
    {
        Sent, // Sent or not sent
        From_email, // _email postfix added to avoid collision with sql FROM keyword
        To_email, 
        Subject,
        Body,
        VideoLink,
        VideoFile
    }

    public readonly string tableName = null;

    /// <summary>
    /// Model class to handle emails database
    /// </summary>
    /// <param name="tableName"></param>
    /// <param name="dbName"></param>
    public EmailsDB(string tableName, string dbName) : base(dbName)
    {
        this.tableName = tableName;

        IDbCommand cmnd = GetDBCommand();
        cmnd.CommandText = string.Format("CREATE TABLE IF NOT EXISTS {0} ({1} INTEGER, {2} TEXT, {3} TEXT, {4} TEXT, {5} TEXT, {6} TEXT, {7} TEXT)",
            tableName,
            EmailsDBColumns.Sent.ToString(),
            EmailsDBColumns.From_email.ToString(),
            EmailsDBColumns.To_email.ToString(),
            EmailsDBColumns.Subject.ToString(),
            EmailsDBColumns.Body.ToString(),
            EmailsDBColumns.VideoLink.ToString(),
            EmailsDBColumns.VideoFile.ToString()
            );
        Debug.Log(cmnd.CommandText);
        cmnd.ExecuteNonQuery();       

    }

    /// <summary>
    /// Inserts a new email into the database
    /// </summary>
    /// <param name="email"></param>
    public void AddData(EmailInfo email)
    {
        IDbCommand cmnd = GetDBCommand();
        cmnd.CommandText = string.Format("INSERT INTO {0} ({1}, {2}, {3}, {4}, {5}, {6}, {7}) VALUES (\"{8}\", \"{9}\", \"{10}\", \"{11}\", \"{12}\", \"{13}\", \"{14}\")",
            tableName,
            EmailsDBColumns.Sent.ToString(),
            EmailsDBColumns.From_email.ToString(),
            EmailsDBColumns.To_email.ToString(),
            EmailsDBColumns.Subject.ToString(),
            EmailsDBColumns.Body.ToString(),
            EmailsDBColumns.VideoLink.ToString(),
            EmailsDBColumns.VideoFile.ToString(),
            ((email.status == EmailStatus.Sent) ? 1 : 0).ToString(),
            email.from, // escape @ character
            email.to,
            email.subject,
            email.body.Replace("\"", "&quot;"),
            (email.attachments_download != null && email.attachments_download.Count  > 0) ? email.attachments_download[0] : "null",
            (email.attachments.Count > 0) ? email.attachments[0] : "null"
            );
        Debug.Log(cmnd.CommandText);
        cmnd.ExecuteNonQuery();
    }

    /// <summary>
    /// Gets a list of all the not sent yet emails in the database
    /// </summary>
    /// <returns>The list of emails that has not been sent yet</returns>
    public List<EmailInfo> GetNotSendedEmails()
    {
        IDbCommand cmnd = GetDBCommand();
        cmnd.CommandText = string.Format("SELECT rowid, {0}, {1}, {2}, {3}, {4}, {5}, {6} FROM {7} WHERE {0} = 0",
            EmailsDBColumns.Sent.ToString(),
            EmailsDBColumns.From_email.ToString(),
            EmailsDBColumns.To_email.ToString(),
            EmailsDBColumns.Subject.ToString(),
            EmailsDBColumns.Body.ToString(),
            EmailsDBColumns.VideoLink.ToString(),
            EmailsDBColumns.VideoFile.ToString(),
            tableName
            );

        IDataReader reader = cmnd.ExecuteReader();
        List<EmailInfo> emails = new List<EmailInfo>();
        while (reader.Read())
        {
            EmailInfo email = new EmailInfo();
            email.rowid = reader.GetInt32(0);
            email.status = (reader.GetInt32(1) == 1) ? EmailStatus.Sent : EmailStatus.NotSentYet;
            email.from = reader.GetString(2);
            email.to = reader.GetString(3);
            email.subject = reader.GetString(4);
            email.body = reader.GetString(5).Replace("&quot;", "\"");

            // adds the uploaded video's download link
            email.attachments_download = new List<string>();
            string videolink = (reader.GetString(6) == "null") ? null : reader.GetString(6);
            if (videolink != null)
                email.attachments_download.Add(videolink);

            // Adds the localfile path
            string videofile = (reader.GetString(7) == "null") ? null : reader.GetString(7);
            email.attachments = new List<string>();
            if (videofile != null)
                email.attachments.Add(videofile);

            // add the constructed email to the emails list
            emails.Add(email);
        }

        return emails;
    }

    public List<EmailInfo> GetAllEmails()
    {
        IDbCommand cmnd = GetDBCommand();
        cmnd.CommandText = string.Format("SELECT rowid, {0}, {1}, {2}, {3}, {4}, {5}, {6} FROM {7}",
            EmailsDBColumns.Sent.ToString(),
            EmailsDBColumns.From_email.ToString(),
            EmailsDBColumns.To_email.ToString(),
            EmailsDBColumns.Subject.ToString(),
            EmailsDBColumns.Body.ToString(),
            EmailsDBColumns.VideoLink.ToString(),
            EmailsDBColumns.VideoFile.ToString(),
            tableName
            );
        Debug.Log(cmnd.CommandText);
        IDataReader reader = cmnd.ExecuteReader();
        List<EmailInfo> emails = new List<EmailInfo>();
        int i = 0;
        while (reader.Read())
        {
            Debug.Log("Reading Email " + (i++));
            EmailInfo email = new EmailInfo();
            email.rowid = reader.GetInt32(0);
            email.status = (reader.GetInt32(1) == 1) ? EmailStatus.Sent : EmailStatus.NotSentYet;
            email.from = reader.GetString(2);
            email.to = reader.GetString(3);
            email.subject = reader.GetString(4);
            email.body = reader.GetString(5).Replace("&quot;", "\"");

            // adds the uploaded video's download link
            email.attachments_download = new List<string>();
            string videolink = (reader.GetString(6) == "null") ? null : reader.GetString(6);
            if (videolink != null)
                email.attachments_download.Add(videolink);
            
            // Adds the localfile path
            string videofile = (reader.GetString(7) == "null") ? null : reader.GetString(7);
            email.attachments = new List<string>();
            if (videofile != null)
                email.attachments.Add(videofile);

            // add the constructed email to the emails list
            emails.Add(email);
        }
        return emails;
    }

    /// <summary>
    /// Updates the email with the given rowid
    /// </summary>
    /// <param name="email"></param>
    public void UpdateEmail(EmailInfo email)
    {
        if (email.rowid == -1)
            AddData(email);
        else
        {
            IDbCommand cmnd = GetDBCommand();
            cmnd.CommandText = string.Format("UPDATE {0} SET {1} = \"{2}\", {3} = \"{4}\",  {5} = \"{6}\", {7} = \"{8}\", {9} = \"{10}\", {11} = \"{12}\", {13} = \"{14}\" WHERE rowid = {15}", 
                tableName, // 0

                EmailsDBColumns.Sent.ToString(), // 1
                ((email.status == EmailStatus.Sent) ? 1 : 0).ToString(), // 2

                EmailsDBColumns.From_email.ToString(), // 3
                email.from, // 4

                EmailsDBColumns.To_email.ToString(), // 5
                email.to, // 6

                EmailsDBColumns.Subject.ToString(), // 7
                email.subject, // 8

                EmailsDBColumns.Body.ToString(), // 9
                email.body.Replace("\"", "&quot;"), // 10

                EmailsDBColumns.VideoLink.ToString(), // 11
                (email.attachments_download != null && email.attachments_download.Count > 0) ? email.attachments_download[0] : "null" , // 12

                EmailsDBColumns.VideoFile.ToString(), // 13
                (email.attachments != null && email.attachments.Count > 0) ? email.attachments[0] : "null", // 14

                email.rowid.ToString() // 15
                );

            Debug.Log(cmnd.CommandText);
            cmnd.ExecuteNonQuery();
        }
    }

    /// <summary>
    /// Removes the table
    /// </summary>
    public void DeleteAllData()
    {
        Debug.Log("Deleting Table...");
        base.DeleteAllData(tableName);
        Debug.Log("Table deleted successfully");
    }

}
