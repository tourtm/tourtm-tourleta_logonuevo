﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data;
using Mono.Data.Sqlite;
using System.IO;

/// <summary>
/// Tutorial: https://medium.com/@rizasif92/sqlite-and-unity-how-to-do-it-right-31991712190
/// </summary>
public class HelloSQLite : MonoBehaviour {
    string connectionURI = null;
	// Use this for initialization
	void Start () {
        connectionURI = "URI=file:" + Application.persistentDataPath + "/My_Database3.sqlite";
        // Open your database connection; if the database name (My_Database.sqlite) does not exist it will be created.
        IDbConnection dbcon = new SqliteConnection(connectionURI);
        dbcon.Open();

        // Creating table if not exists
        IDbCommand dbcmd;
        IDataReader reader;
        dbcmd = dbcon.CreateCommand();
        string q_createTable =
  "CREATE TABLE IF NOT EXISTS my_table (val INTEGER )";
        dbcmd.CommandText = q_createTable;
        // Executes the command
        reader = dbcmd.ExecuteReader();        

        // Read the table
        IDbCommand cmnd_reader = dbcon.CreateCommand();
        cmnd_reader.CommandText = "SELECT rowid, val FROM my_table";
        reader = cmnd_reader.ExecuteReader();

        while (reader.Read())
        {
            Debug.Log("ID: " + reader[0].ToString());
            Debug.Log("VAL: " + reader[1].ToString());
        }
        dbcon.Close();
    }

    public void InsertRow()
    {
        IDbConnection dbcon = new SqliteConnection(connectionURI);
        dbcon.Open();
        // Inserting rows
        IDbCommand cmnd = dbcon.CreateCommand();
        cmnd.CommandText = string.Format("INSERT INTO my_table (val) VALUES ({0})", Random.Range(1, 15));
        cmnd.ExecuteNonQuery();
        dbcon.Close();
    }
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.S))
        {
            InsertRow();
        }
	}
}
