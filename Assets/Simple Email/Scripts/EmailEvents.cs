﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace EmailSender
{
    [System.Serializable]
    public class EmailEvent : UnityEvent<EmailInfo>
    {

    }

    [RequireComponent(typeof(EmailMessage))]
    public class EmailEvents : MonoBehaviour
    {
        [HideInInspector]
        public RemainingEmails pendingEmails;

        [Header("Events")]
        public EmailEvent OnSendingStart;
        System.Action<EmailInfo> onSendingStartDeleg;

        public EmailEvent OnSent;
        System.Action<EmailInfo> onSentDeleg;

        public EmailEvent OnFailed;
        System.Action<EmailInfo> onFailedDeleg;

        // Use this for initialization
        void Start()
        {
            onSendingStartDeleg = OnSendingStartMethod;
            pendingEmails.OnSendingStart -= onSendingStartDeleg;
            pendingEmails.OnSendingStart += onSendingStartDeleg;

            onFailedDeleg = OnEmailFailed;
            pendingEmails.OnEmailFail -= onFailedDeleg;
            pendingEmails.OnEmailFail += onFailedDeleg;

            onSentDeleg = OnEmailSent;
            pendingEmails.OnEmailSuccess -= onSentDeleg;
            pendingEmails.OnEmailSuccess += onSentDeleg;
        }

        // Update is called once per frame
        void Update()
        {

        }

        private void OnDestroy()
        {
            pendingEmails.OnSendingStart -= onSendingStartDeleg;
            pendingEmails.OnEmailSuccess -= onSentDeleg;
            pendingEmails.OnEmailFail -= onFailedDeleg; // TODO: Read more about delegates for know the implications with static members
        }

        /// <summary>
        /// gets called when the EmailSenderSender.SendAsync() method gets called
        /// </summary>
        public void OnSendingStartMethod(EmailInfo email)
        {
            OnSendingStart.Invoke(email);
        }

        /// <summary>
        /// Called when the email is sent
        /// </summary>
        /// <param name="email"></param>
        public void OnEmailSent(EmailInfo email)
        {
            OnSent.Invoke(email);
        }

        /// <summary>
        /// Called when the email failed to be sent
        /// </summary>
        public void OnEmailFailed(EmailInfo email)
        {
            Debug.Log("Email failed");
            OnFailed.Invoke(email);
        }

    }

}

