﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net.Mail;
using System.IO;

namespace EmailSender
{
    public enum EmailStatus
    {
        NotSentYet,
        Sent        
    }

    /// <summary>
    /// This class holds references to pending emails to be sent.
    /// </summary>
    [CreateAssetMenu(fileName = "remaining-emails.asset", menuName = "Remaining Email List")]
    public class RemainingEmails : ScriptableObject
    {
        public event System.Action<EmailInfo> OnSendingStart;
        public event System.Action<EmailInfo> OnEmailSuccess;
        public event System.Action<EmailInfo> OnEmailFail;

        // Pending email list
        public List<EmailInfo> pendingEmails;

        public EmailInfo lastAdded = null;

        // Add pending emails
        public void AddPendingEmail(MailMessage email, List<string> attachedFiles)
        {
            lastAdded = new EmailInfo { status = EmailStatus.NotSentYet, Message = email, sendAttemps = 0, attachments = attachedFiles };
            // By default a email that has recently added is not sent yet
            pendingEmails.Add(lastAdded);
#if UNITY_EDITOR
            //UnityEditor.EditorUtility.SetDirty(this);
#endif
        }

        // TODO: Show in inspector email actual status just like: Sending... Succesfully sended.
        public void SendLastAddedEmail()
        {
            if (lastAdded == null) return;

            if (lastAdded.status == EmailStatus.NotSentYet)
            {
                SimpleEmailSender.SendAsync(lastAdded, OnSucces, OnFail, OnSendingStartMethod);
            }
        }

        /// <summary>
        /// This coroutine try to send emails that has been added to the pendingEmail list.
        /// </summary>
        public IEnumerator TryToSendPendingEmails()
        {
            WaitForSeconds wait = new WaitForSeconds(1f);
            yield return wait;

            // Iterates over the list of pending emails
            for (int i = 0; i < pendingEmails.Count; i++)
            {
                // Checks the pending email list
                if (pendingEmails[i].status == EmailStatus.NotSentYet)
                {
                    // Updates the auth info to the actual one
                    yield return wait;
                    SimpleEmailSender.SendAsync(pendingEmails[i], OnSucces, OnFail, OnSendingStartMethod);
                }
            }
        }

        public void OnSendingStartMethod(EmailInfo sendedEmail)
        {
            if (OnSendingStart != null)
                OnSendingStart(sendedEmail);
        }

        public void OnSucces(EmailInfo sendedEmail)
        {
            // On success to sent email
            sendedEmail.status = EmailStatus.Sent;
            // Removes the email from the pending list
            pendingEmails.Remove(sendedEmail);
            Debug.Log("EmailSender: Email sent successfully!");
            //TODO: Removes the actual email from the list
            // Invokes the OnEmailSent event 
            if (OnEmailSuccess != null)
                OnEmailSuccess(sendedEmail);
        }

        public void OnFail(EmailInfo sendedEmail)
        {
            Debug.Log("Message: " + sendedEmail.Message == null);
            // On failed to sent email
            sendedEmail.status = EmailStatus.NotSentYet;
            Debug.Log("EmailSender: An error occurred while sending the email :(");
            // Increments by one the number of attempts
            sendedEmail.sendAttemps++;

            // Invokes the OnEmailFailed event
            if (OnEmailFail != null)
                OnEmailFail(sendedEmail);
        }
    }

    [System.Serializable]
    public class EmailInfo
    {
        /// <summary>
        /// Ir rowid == -1 then this email is not saved on the database
        /// </summary>
        public int rowid = -1;
        public EmailStatus status;
        /// <summary>
        /// How much times this email has been tried to be sent wit no success.
        /// 
        /// After of three failed attempts this email is deleted.
        /// </summary>
        public int sendAttemps = 0;

        public string from = null;
        public string to = null;
        public string subject = null;
        public bool isBodyHtml = true;
        public string body = null;
        /// <summary>
        /// The download link of uploaded attachments
        /// </summary>
        public List<string> attachments_download = null; // Add manually
        /// <summary>
        /// The local URI of the attachments
        /// </summary>
        public List<string> attachments = null; // Add manually

        public MailMessage Message {
            // Instantiate a new MailMessage using the supplied parameters
            get
            {
                if (from == null)
                {
                    // TODO: Show friendly message in inspector telling to user that please supply a user email
                    // TODO: Make re-gex email validation.
                    return null;
                }
                // Mail message
                MailMessage msg = new MailMessage(from, to);
                if (subject == null || subject == "")
                {
                    // TODO: Show to user in inspector that should supply a valid subject
                    return null;
                }
                msg.Subject = subject;

                if (body == null || body == "")
                {
                    // TODO: Show to user in inspector that should supply a valid body
                    return null;
                }
                msg.IsBodyHtml = isBodyHtml;
                msg.Body = body;

                //if (attachments != null && attachments.Count > 0) // checks there are attached files
                //{
                //    foreach (string file in attachments) // Attach all attached files
                //    {
                //        if (file != null && !file.Equals(""))
                //            if (File.Exists(file))
                //                msg.Attachments.Add(new Attachment(file));
                //    }
                //}

                return msg;
            }

            set
            {
                MailMessage msg = value;
                from = msg.From.ToString();
                to = msg.To.ToString();
                subject = msg.Subject;
                isBodyHtml = msg.IsBodyHtml;
                body = msg.Body;

                // Do nothing with attachments
                //if (attachments == null)
                //    attachments = new List<string>();
                //else
                //    attachments.Clear();

                //foreach (Attachment attachment in msg.Attachments)
                //{
                //    attachments.Add(attachment.);
                //}
            }
        }

        public override string ToString()
        {
            return string.Format("rowid = {0} | status = {1} | from = {2} | to = {3} | subject = {4} | attachment[0] = {5} | attachment_downloads[0] = {6} | isBodyHtml = {7} | body = {8}",
                rowid,
                status,
                from,
                to, 
                subject,
                attachments[0],
                attachments_download[0],
                isBodyHtml,
                body
                );
        }
    }
}
