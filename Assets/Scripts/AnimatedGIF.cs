﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace App
{
    [RequireComponent(typeof(Image))]
    public class AnimatedGIF : MonoBehaviour
    {

        public List<Sprite> frames;
        public float framesPerSecond = 10f;
        private Image img;

        // Use this for initialization
        void Start()
        {
            img = GetComponent<Image>();
        }

        // Update is called once per frame
        void Update()
        {
            int index = (int) ((Time.time * framesPerSecond) % frames.Count);
            img.sprite = frames[index];
        }
    }

}
