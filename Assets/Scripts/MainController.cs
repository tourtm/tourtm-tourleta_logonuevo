﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RockVR.Video;
using App;
using UnityEngine.Events;
using Dweiss;


public class MainController : MonoBehaviour
{

    public Spinner StopController;
    public GameObject GUIManager;
    public GameObject ChallengeTimerText;
    public GameObject ChallengeLoading;
    public GameObject ChallengeTiming;
    public bool OneTimeCall = true;
    public bool Challengetime = false;
    public bool CameraTime = false;
    public bool VideoEmail = false;
    public GameObject CameraView;
    public GameObject AudioManager;
    public GameObject VideoEmailPlayer;
    public List<GameObject> VideoPlayerUI;
    public float PreparationCountdown = 5;
    public float ChallangeTime = 13;

    [Header("OBS Connection Data")]
    public string Host;
    public int Port;
    public string Password;
    /// <summary>
    /// Invoked when obs open comand is executed
    /// </summary>
    public UnityEvent OnOBSOpenStart;
    /// <summary>
    /// Invoked when obs has correctly openeted and initialized its websocket server
    /// </summary>
    public UnityEvent OnOBSOpenCompleted;


    // Use this for initialization
    void Start()
    {
        // Register OBS open events
        OBSBridge.Instance.OnOBSOpenStart -= OnOBSOpenStartHandler;
        OBSBridge.Instance.OnOBSOpenStart += OnOBSOpenStartHandler;
        OBSBridge.Instance.OnOBSOpenCompleted -= OnOBSOpenCompletedHandler;
        OBSBridge.Instance.OnOBSOpenCompleted += OnOBSOpenCompletedHandler;

        // TODO: Implement pause mechanism to wait obs to open.

        Host = Settings.Instance.OBSWebsocketHost;
        Port = Settings.Instance.OBSWebsocketPort;
        Password = Settings.Instance.OBSWebsocketPassword;
        OBSBridge.Instance.Init(Host, Port, Password);
    }

    void OnOBSOpenStartHandler()
    {
        Debug.Log("OnOBSOpenStartHandler");
        OnOBSOpenStart.Invoke();
    }

    void OnOBSOpenCompletedHandler()
    {
        Debug.Log("OnOBSOpenCompletedHandler");
        OnOBSOpenCompleted.Invoke();
    }

   IEnumerator TimerController(float seconds)
   {
        Debug.Log("time" + Time.time);
        for (int t=0; t <= seconds; t++)
        {
            ChallengeTimerText.GetComponent<Text>().text = ((int)(seconds - t)).ToString();
            ChallengeLoading.GetComponent<Image>().fillAmount = ((seconds - t) / seconds);
            yield return new WaitForSeconds(1);
        }
        
        Debug.Log("time" + Time.time);
        Challengetime = true;
   }

 
    /// <summary>
    /// Shows email send screen.
    /// </summary>
    /// <param name="secondsC">Seconds to wait until the email screen is shown.</param>
    /// <returns></returns>
    IEnumerator CameraTimer(float secondsC)
    {
        yield return new WaitForSeconds(secondsC);
        CameraView.SetActive(false);  //Deactivates the camera
        AudioManager.GetComponent<AudioController>().MicDesactive();
        GUIManager.GetComponent<GUIManager>().MakeInvisibleAllChallengesTemplates();
        Debug.Log("time camera" + Time.time);
        CameraTime = true;
        VideoEmailPlayer.SetActive(true);
        VideoPlayerUI[4].SetActive(false);// set active false the player button; child of the videoSenderPlayer**********
        //VideoPlayerUI[3].SetActive(false); //set active false the restart button to make sure the app isn't stoped while processing the video
        yield return new WaitForSeconds(1); // wait 1 second to set VideoEmail true.
        VideoEmail = true;
        CameraViewer.Instance.PauseCamera(); 
    }

    IEnumerator ChallengeTimer(float secondsClock)
    {
        for (int t = 0; t <= secondsClock; t++)
        {
            float fillAmount = ((secondsClock - t) / secondsClock);
            ChallengeTiming.GetComponent<Image>().fillAmount = fillAmount;
            yield return new WaitForSeconds(1);
        }
    }
    
    // Update is called once per frame
    void Update()
    {
        GUIManager.GetComponent<GUIManager>().RecorderUI();

        if (Spinner.isStoped == true)
        {
            
            if (OneTimeCall)
            {
                StartCoroutine(TimerController(PreparationCountdown));
                //StartCoroutine(CameraTimer(19));
                //StartCoroutine(ChallengeTimer(19));
                GUIManager.GetComponent<GUIManager>().ChallengeUI();
                OneTimeCall = false;
                CameraViewer.Instance.PlayCamera();  //Call the playCamera method. ************
                //GUIManager.GetComponent<GUIManager>().RecorderUI();
            }

            // true in the first frame in which camera start recording
            if(Challengetime)
            {
                //VideoCaptureCtrl.instance.StartCapture();
                OBSBridge.Instance.StartRecording();
                Debug.LogFormat("<color=purple>StartRecording</color>");
                CameraView.SetActive(true);  //Activates the camera
                GUIManager.GetComponent<GUIManager>().ChallengeTemplateUI(); //Activates the template accorging the challenge over the camera 
                Challengetime = false;
                StartCoroutine(CameraTimer(ChallangeTime));
                StartCoroutine(ChallengeTimer(ChallangeTime));
                //AudioManager.GetComponent<AudioController>().MicActive(); // Mic being activated
            }

            // True in the frame in wich recording should stop
            if(CameraTime)
            {
                //VideoCaptureCtrl.instance.StopCapture();
                OBSBridge.Instance.StopRecording();
                Debug.LogFormat("<color=purple>StopRecording</color>");
                AudioManager.GetComponent<AudioController>().MicDesactive();
                //CameraView.SetActive(false);
                CameraTime = false;
            }

            /*if(VideoEmail)
            {
                if (VideoCaptureCtrl.instance.status == VideoCaptureCtrl.StatusType.FINISH)
                {
                    VideoPlayer.instance.SetRootFolder();
                    VideoPlayer.instance.PlayVideo(VideoPlayer.instance.videoFiles[VideoPlayer.instance.videoFiles.Count - 1]); ///////custome line
                }
            }*/
        }
    }
}




