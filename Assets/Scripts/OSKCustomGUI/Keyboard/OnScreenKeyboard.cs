﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnScreenKeyboard : MonoBehaviour {
    public bool isVisible;
    Animator animator;
	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnVisibleState()
    {
        isVisible = true;
    }

    public void OnHiddenState()
    {
        isVisible = false;
    }

    public void Show()
    {
        if (!isVisible)
            animator.SetTrigger("show");
    }

    public void Hide()
    {
        if (isVisible)
            animator.SetTrigger("hide");
    }
}
