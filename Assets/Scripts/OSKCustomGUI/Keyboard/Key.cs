﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

[ExecuteInEditMode]
public class Key : MonoBehaviour {
    // it seems like the IPointerDownHandler interface doesn't work well on the "marco tactil"
    public KeyboardState keyboardState;

    public float keyLockSeconds = 0.3f;
    public float timeOfLastPress;
    public bool isLocked = false;

    /// <summary>
    /// Default key to show when mayus is disabled
    /// </summary>
    public string key;
    /// <summary>
    /// Alternative key to show when mayus is enabled
    /// </summary>
    public string altKey;
    public bool hasAlternativeKey;
    /// <summary>
    /// WHeter this key is used for control keyboard or not
    /// </summary>
    public bool isControlKey;

    [Header("GUI Options")]
    public int fontSize = 24;
    public bool showUpperInUI = true;
    public Color fontColor = Color.white;
    public Color onSelected;
    public Color normalColor;
    Button button;
    Image image;
    Text text;


    // Use this for initialization
    void Start () {
        button = GetComponent<Button>();
        image = GetComponent<Image>();
        normalColor = image.color;
        text = button.GetComponentInChildren<Text>();
        text.fontSize = fontSize;
        if (!isControlKey)
        {
            text.text = showUpperInUI ? key.ToUpper() : key.ToLower();
        }
        text.color = fontColor;

        keyboardState.OnStateChange.AddListener(OnKeyboardStateChange);
        // register to button on click
        button.onClick.AddListener(OnPointerDown);
    }
	
	// Update is called once per frame
	void Update () {
        // is locked
        if (isLocked)
            isLocked = (Time.time - timeOfLastPress) < keyLockSeconds;
    }

    private void OnDestroy()
    {
        button.onClick.RemoveListener(OnPointerDown);
    }

    void OnKeyboardStateChange(KeyboardState newState)
    {
        if (newState.isMayusEnabled)
        {
            if (key == "shift")
            {
                image.color = onSelected;
            }
        }
        else
        {
            if (key == "shift")
            {
                image.color = normalColor;
            }
        }

             
        if (newState.isShowingNumbers)
        {
            if (key == "show_alt")
            {
                image.color = onSelected;
            }

            if (hasAlternativeKey)
                text.text = altKey;
                
        }
        else
        {
            if (key == "show_alt")
            {
                image.color = normalColor;
            }

            if (hasAlternativeKey)
                text.text = key.ToUpper();
        }
    }

    public void OnPointerDown()
    {
        if (isLocked)
        {
            return;
        }
        else
        {
            timeOfLastPress = Time.time;
            isLocked = true;
        }
        string key = this.key;
        Debug.Log(key);
        if (keyboardState.isShowingNumbers && hasAlternativeKey)
        {
            key = this.altKey;
        }

        if (!isControlKey)
            key = keyboardState.isMayusEnabled ? key.ToUpper() : key.ToLower();
        KeyCommand keyCommand = new KeyCommand(key, isControlKey);
        keyboardState.InformKeyPress(keyCommand);
    }
}
