﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions
{
    public static Color FromHex(int hexColor)
    {
        float r = (hexColor & 0xff0000) >> 4;
        float g = (hexColor & 0x00ff00) >> 2;
        float b = hexColor & 0x0000ff;
        float a = 255f;

        return new Color(r, g, b, a);
    }
}