﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class StateEvent : UnityEvent<KeyboardState> { }
// This is an inmutable struct to prevent a command to be changed
public struct KeyCommand
{
    private readonly string key;
    public string Key
    {
        get
        {
            return key;
        }
    }
    private readonly bool isControl;
    public bool IsControl
    {
        get
        {
            return isControl;
        }
    }

    public KeyCommand(string key, bool isControl)
    {
        this.key = key;
        this.isControl = isControl;
    }

    public override string ToString()
    {
        return "key: " + key + " | isControl: " + isControl;
    }
}

public interface IKeyboardEvent
{
    void OnKeyPressed(KeyCommand keyCommand);
}

[CreateAssetMenu(fileName = "keyboardState.asset", menuName = "Telemedellin/Keyboard State")]
public class KeyboardState : ScriptableObject {
    public StateEvent OnStateChange = new StateEvent();
    // Listeners
    private readonly List<IKeyboardEvent> eventListeners =
        new List<IKeyboardEvent>();
    public bool isShowingNumbers;
    public bool isMayusEnabled;

    public void ToggleNumbers()
    {
        isShowingNumbers = !isShowingNumbers;
        OnStateChange.Invoke(this);
    }

    public void ToggleMayus()
    {
        isMayusEnabled = !isMayusEnabled;
        OnStateChange.Invoke(this);
    }

    public void InformKeyPress(KeyCommand key)
    {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnKeyPressed(key);
    }

    public void RegisterListener(IKeyboardEvent listener)
    {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(IKeyboardEvent listener)
    {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}
