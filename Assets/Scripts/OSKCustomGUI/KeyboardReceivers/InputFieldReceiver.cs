﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(InputField))]
public class InputFieldReceiver : MonoBehaviour, IPointerClickHandler, IKeyboardEvent
{
    public static int LastFocusedId;
    public KeyboardState keyboardState;
    InputField inputField;
    public string currentText;
    public bool FocusOnStart;

	// Use this for initialization
	void Start () {
        keyboardState.RegisterListener(this);
        
        inputField = GetComponent<InputField>();
        currentText = string.Empty;

        if (FocusOnStart)
            LastFocusedId = GetInstanceID();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnDestroy()
    {
        keyboardState.UnregisterListener(this);
        LastFocusedId = -1;
    }

    public void ClearText()
    {
        currentText = string.Empty;
    }

    public void OnKeyPressed(KeyCommand keyCommand)
    {
        if (LastFocusedId != GetInstanceID()) return;
        if (!keyCommand.IsControl)
        {
            currentText += keyCommand.Key;
            inputField.caretPosition = currentText.Length - 1;
        }
        else
        {
            switch (keyCommand.Key)
            {
                case "return":
                    int index = Mathf.Max(0, currentText.Length - 1);
                    if (index < currentText.Length)
                        currentText = currentText.Remove(index);
                    break;
            }
        }

        inputField.text = currentText;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        LastFocusedId = GetInstanceID();
    }
}
