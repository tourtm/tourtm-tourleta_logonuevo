﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OSKBehaviour : MonoBehaviour {

    public VirtualKeyboard keyboard = null;
    public Rect keyboardPos;

    // Use this for initialization
    void Start () {
        Application.runInBackground = true;

        // Initializes the keyboard
        if (keyboard == null)
            keyboard = new VirtualKeyboard();
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            ShowKeyboard();
        }
        else if(Input.GetKeyDown(KeyCode.Alpha2))
        {
            HideKeyboard();
        }
	}

    public void ShowKeyboard()
    {
        keyboard.ShowTouchKeyboard();
    }

    public void HideKeyboard()
    {
        keyboard.HideTouchKeyboard();
    }

    public void RepositioniateKeyboard(Rect rect)
    {
        keyboard.RepositionOnScreenKeyboard(rect);
    }
}
