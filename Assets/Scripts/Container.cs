﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Container : MonoBehaviour {

    public GameObject content;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ShowContent()
    {
        content.SetActive(true);
    }
    public void HideContent()
    {
        content.SetActive(false);
    }
}
