﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RockVR.Common;


public class GUIManager : MonoBehaviour {

    public Spinner spinner;
    public float StarterTimer = 5f;
    public float ChallengeTime = 10f;
    public List<GameObject> ChallegeScreen;
    public Needle SlotCheck;
    public MainController MController;
    public GameObject ChallengeManager;
    public GameObject ChallengeBar;
    public GameObject CameraPre;
    public GameObject VideoPlayer;
    public GameObject VideoEmailPlayer;
    public GameObject AudioManager;
    public List<GameObject> ChallegeTemplates;
    public GameObject ChallengeClock;
    AudioSource KaraokeDanceSong;
    public GameObject ChallengeTemplate;
    public GameObject RuletaBoton;
    public GameObject BottomBar;

    // Use this for initialization
    void Start () {

        KaraokeDanceSong = GetComponent<AudioSource>();

    } 
	
	// Update is called once per frame
	void Update () {

    }

    public void RecorderUI()
    {
        if (MController.Challengetime == true)
        {
            ChallengeManager.SetActive(false);
            ChallengeBar.SetActive(false);
            
        }
    }

    public void ChallengeUI()
    {
        ChallengeManager.SetActive(true);
        RuletaBoton.SetActive(false);
        ChallengeBar.SetActive(true);
        CameraPre.SetActive(false);
        ChallengeClock.SetActive(false);
        VideoEmailPlayer.SetActive(false);
        // set active false the player button; child of the videoSenderPlayer********** (videoemailplayer)
        VideoPlayer.SetActive(false);


        for (int i = 1; i < ChallegeScreen.Count; i++)
        {
            if (SlotCheck.SpinPosition == i)
            {
                MakeInvisibleAllChallenges();
                // Makes visible the actual challenge
                SetActiveChallenges(i, true);
                
            }
        }
    }
    
    void MakeInvisibleAllChallenges()
    {
        // Iterates over all the challenge entities
        for (int i = 1; i < ChallegeScreen.Count; i++)
        {
            
            SetActiveChallenges(i, false);
            ChallegeScreen[i].SetActive(false);
        }
    }

    void SetActiveChallenges(int i, bool value)
    {
       
        for (int j = 1; j < ChallegeScreen.Count; j++)
        {
            ChallegeScreen[i].SetActive(value);  //Activates the challenge according to the spinner slot
        }
    }

    public void CameraManager()
    {
        CameraPre.SetActive(true);
    }

    // Challenge Templates
    ////////////////////

    public void MakeInvisibleAllChallengesTemplates()
    {
        // Iterates over all the challenge entities
        for (int i = 1; i < ChallegeTemplates.Count; i++)
        {

            SetActiveChallengesTemplates(i, false);
            ChallegeTemplates[i].SetActive(false);
        }
    }

    void SetActiveChallengesTemplates(int i, bool value)
    {

        ChallegeTemplates[i].SetActive(value);
        if (i == 5) // the only template with an animation is this.
        {
            BottomBar.SetActive(true); // show the bottom bar.
        }

        // The karaoke song should play in these both scenes
        if (i == 5 || i == 7)
        {
            //Snapshots for the karaoke 
            AudioManager.GetComponent<AudioController>().KaraokeSong();

            //playsong *********
            KaraokeDanceSong.Play(0);
        }
        else
        {
            AudioManager.GetComponent<AudioController>().MicActive();
        }
    }

    public void ChallengeTemplateUI()
    {
        ChallengeClock.SetActive(true);
        ChallengeTemplate.SetActive(true);
        MakeInvisibleAllChallengesTemplates();
        // Makes visible the actual challenge
        // shows the subtitle
        // SetActiveChallengesTemplates(7, true);
        // TODO: Uncomment this
        SetActiveChallengesTemplates(SlotCheck.SpinPosition, true);
    }
}



