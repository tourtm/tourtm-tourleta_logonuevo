﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OBSWebsocketDotNet;

public class TestingOBS : MonoBehaviour {

    OBSWebsocket obs = null;
    public string Host = "localhost";
    public int Port = 4444;
    public string Password = "1234";
	// Use this for initialization
	void Start () {
        // ws is the url schema
        // see this wikipedia link to get more info about url syntax: https://en.wikipedia.org/wiki/URL#Syntax
        string url = "ws://" + Host + ":" + Port;
        obs = new OBSWebsocket();
        obs.Connect(url, Password);
        OutputStatus streamingStatus = obs.GetStreamingStatus();
        Debug.Log(streamingStatus.IsRecording);
	}

    // toggle recording
    [ContextMenu("ToggleRecording()")]
    public void ToggleRecording()
    {
        obs.ToggleRecording();
    }

    // Update is called once per frame
    void Update () {
		
	}

    private void OnDestroy()
    {
        // disconnects this client instance from the OBS server.
        obs.Disconnect();
    }
}
