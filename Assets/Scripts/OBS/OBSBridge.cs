﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.Diagnostics;
using System.IO;
using OBSWebsocketDotNet;
using Dweiss; // Settings file utilities

namespace App
{

    /// <summary>
    /// 
    /// </summary>
    public enum OS
    {
        Unknown, // Api was no able to detect the OS
        Windows,
        OSX,
        Linux
    };

    /// <summary>
    /// Describes the state of an output (streaming or recording)
    /// </summary>
    public enum OutputStateExtended
    {
        /// <summary>
        /// The output is not in any of the posible states
        /// </summary>
        Nothing,

        /// <summary>
        /// The output is initializing and doesn't produces frames yet
        /// </summary>
        Starting,

        /// <summary>
        /// The output is running and produces frames
        /// </summary>
        Started,

        /// <summary>
        /// The output is stopping and sends the last remaining frames in its buffer
        /// </summary>
        Stopping,

        /// <summary>
        /// The output is completely stopped
        /// </summary>
        Stopped
    }


    // TODO: Set recording folder by sending the request to the websocket server
    // see: https://github.com/Palakis/obs-websocket-dotnet/blob/076c6fd115d8bba8fae9ce7545565c2ecd8dc3ed/obs-websocket-dotnet/OBSWebsocket_Requests.cs#L386
    public partial class OBSBridge : Singleton<OBSBridge>
    {
        public string Host;
        public int Port;
        public string Password;

        private string obs64ProcessName = "obs64";
        private string obs32ProcessName = "obs32";

        private OBSWebsocket obs;

        public OutputStateExtended CurrentRecordingState;

        /// <summary>
        /// Returns false if websocket client hasn't been instantiated yet
        /// or is not connected yet.
        /// </summary>
        public bool IsAvailable
        {
            get
            {
                if (obs == null)
                {
                    UnityEngine.Debug.Log("websocket client is not instantited yet. Try by calling ConnectWebsocket() method.");
                    return false;
                }

                if (!obs.IsConnected)
                {
                    UnityEngine.Debug.Log("websocket client is not conected to the obs websocket server. Try by calling ConnectWebsocket() method.");
                    return false;
                }

                return true;
            }
        }

        public string Url
        {
            get
            {
                // TODO: DO the followings checks to Host and Port.
                // 1. port not zero.
                // 2. host is not null.
                // 3. host is not an empty string.
                // ws is the url schema
                // see this wikipedia link to get more info about url syntax: https://en.wikipedia.org/wiki/URL#Syntax
                string url = "ws://" + Host + ":" + Port;
                return url;
            }
        }

        /// <summary>
        /// Connect to obs websocket by using the specified auth data
        /// </summary>
        /// <param name="host"></param>
        /// <param name="port"></param>
        /// <param name="password"></param>
        public void ConnectionData(string host, int port, string password)
        {
            Host = host;
            Port = port;
            Password = password;
        }

        /// <summary>
        /// 1. Opens OBS
        /// 2. Start the ConnectWebsocket coroutine
        /// </summary>
        public void Init(string host, int port, string password)
        {
            ConnectionData(host, port, password);
            OpenOBS();
            StartCoroutine(ConnectWebsocket());
        }

        /// <summary>
        /// Opens the OBS app if is not open yet.
        /// </summary>
        public void OpenOBS()
        {
            // Invoke the OnOBSOpenStart event
            OnOBSOpenStart();
            Process[] obs64OpenInstances = Process.GetProcessesByName(obs64ProcessName);
            Process[] obs32OpenInstances = Process.GetProcessesByName(obs64ProcessName);
            // If there is an open instance of obs64 bits or obs32 bits then there is no need to 
            // open obs again
            if (obs64OpenInstances.Length > 0 || obs32OpenInstances.Length > 0)
            {
                UnityEngine.Debug.Log("There is already an opened instance of OBS studio. No need to open another.");
                return;
            }

            // TODO: Load this from a cfg file, proposal is to use this library:
            // https://github.com/cemdervis/SharpConfig
            string obsPath = Settings.Instance.OBSExecutableFolder;
            string obsExeName = Settings.Instance.OBSExecutableName;
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = obsExeName;
            startInfo.WorkingDirectory = obsPath; // like cd path command
            Process.Start(startInfo);
            // UnityEngine.Debug.Log(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles));
        }

        string GetFolderPath(Environment.SpecialFolder specialFolder)
        {
            return Environment.GetFolderPath(specialFolder);
        }

        /// <summary>
        /// Closes all the opened obs instances
        /// NOT IMPLEMENTED YET
        /// </summary>
        public void CloseOBS()
        {
            UnityEngine.Debug.Log("Not Implemented");
        }

        /// <summary>
        /// Instantiates and connect to the obs websocket server
        /// 
        /// This coroutine will wait until the obs websocket server gets available
        /// this is useful because when the OBS program is started it won't be open instantnly and it will
        /// vary from computer to computer.
        /// </summary>
        public IEnumerator ConnectWebsocket()
        {
            /// Creates a websocket client which will try to connect
            /// to the obs server, this will be used to detects when
            /// obs is ready to listen requests.
            ServerAvailabilityInit();
            yield return new WaitUntil(isWebsocketServerAvailable);
            // At this point OBS should be opened and its websocket server initializeds.
            // Invoke the event.
            OnOBSOpenCompleted();

            // ws is the url schema
            // see this wikipedia link to get more info about url syntax: https://en.wikipedia.org/wiki/URL#Syntax
            string url = Url;
            // checks if a OBSWebsocket instance already exists
            if (obs == null)
            {
                obs = new OBSWebsocket();
            }
            try
            {
                if (!obs.IsConnected)
                {
                    // NOTE: Unity will freeze if we try to connect when the
                    // websocket server is not already started.
                    // See OBSWebsocketDotNet::OBSWebsocket::Connect() method https://github.com/Palakis/obs-websocket-dotnet/blob/076c6fd115d8bba8fae9ce7545565c2ecd8dc3ed/obs-websocket-dotnet/OBSWebsocket.cs#L207
                    // See WebsocketSharp::WebSocket constructor https://github.com/sta/websocket-sharp/blob/beb3a149761ff4aa144037ed2a40725cca02306f/websocket-sharp/WebSocket.cs#L255
                    obs.Connect(url, Password);
                    obs.OBSExit -= OBSExit;
                    obs.OBSExit += OBSExit;

                    obs.RecordingStateChanged -= RecordingStateChanged;
                    obs.RecordingStateChanged += RecordingStateChanged;
                }
                else
                {
                    UnityEngine.Debug.Log("Websocket client already connected to server, don't do anything.");
                }
            }
            catch (System.Exception exception) // I know this is a bad practice to catch exceptions. Sorry for that.
            {
                UnityEngine.Debug.LogError(exception.ToString());
            }
        }

        /// <summary>
        /// TODO: Call this method when obs exits
        /// </summary>
        public void DisconnectWebsocket()
        {

            // If has not been instantiated a Websocket client
            // or has been destroyed elsewhere
            if (!IsAvailable) return;

            // Stops recording
            StopRecording();
            obs.Disconnect();
        }

        public void ToggleRecording()
        {
            if (!IsAvailable) return;

            obs.ToggleRecording();
        }

        public void StartRecording()
        {
            // Checks if obs instance is available
            if (!IsAvailable) return;

            OutputStatus outputStatus = obs.GetStreamingStatus();
            // Start recording only if not currently recording.
            // This check is done because if obs.StartRecording() is called while recording it 
            // will throw an error.
            if (!outputStatus.IsRecording)
            {
                obs.StartRecording();
            }
        }

        public void StopRecording()
        {
            if (!IsAvailable) return;

            OutputStatus outputStatus = obs.GetStreamingStatus();
            // It is checked because it will throw an error if obs is not recording.
            if (outputStatus.IsRecording)
            {
                obs.StopRecording();
            }
        }

        #region Methods To Handle Events
        public void RecordingStateChanged(OBSWebsocket sender, OutputState currentState)
        {
            int currentStateInt = ((int)currentState) + 1;
            CurrentRecordingState = (OutputStateExtended) currentStateInt;
        }

        public void OBSExit(object sender, EventArgs msg)
        {
            // Checks the websocket client is instantiated and connected
            // Don't do anything if obs has not been instantiate or has not been connected yet.
            if (obs == null || !obs.IsConnected) return;
            UnityEngine.Debug.Log("OBS is closing, disconnecting created websocket client instance.");
            DisconnectWebsocket();
        }
        #endregion

        protected override void BeforeOnDestroy()
        {
            // Closes the websocket connection created to test
            // server availability
            DisposeServerAvailability();
            DisconnectWebsocket();
        }
    }
}
