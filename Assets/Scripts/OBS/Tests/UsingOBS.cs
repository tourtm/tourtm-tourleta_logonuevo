﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App
{
    public class UsingOBS : MonoBehaviour
    {
        public string Host;
        public int Port;
        public string Password;

        // Use this for initialization
        void Start()
        {
            // Open a new OBS instance if is not opened yet.
            OpenOBS();
            // Sets connections data
            OBSBridge.Instance.ConnectionData(Host, Port, Password);
        }

        // Update is called once per frame
        void Update()
        {

        }
        
        [ContextMenu("OpenOBS()")]
        public void OpenOBS()
        {
            OBSBridge.Instance.OpenOBS();
        }

        //[ContextMenu("IsSocketServerListening")]
        //public void IsSocketServerListening()
        //{
        //    OBSBridge.Instance.IsSocketServerListening();
        //}

        [ContextMenu("ConnectWebsocket()")]
        public void ConnectWebsocket()
        {
            // this method dont do anything if a client instance is already connected
            StartCoroutine(OBSBridge.Instance.ConnectWebsocket());
        }

        [ContextMenu("DisconnectWebsocket()")]
        public void DisconnectWebsocket()
        {
            // this method dont do anything if a client instance is already disconnected
            OBSBridge.Instance.DisconnectWebsocket();
        }

        [ContextMenu("StartRecording()")]
        public void StartRecording()
        {
            OBSBridge.Instance.StartRecording();
        }

        [ContextMenu("StopRecording()")]
        public void StopRecording()
        {
            OBSBridge.Instance.StopRecording();
        }

        [ContextMenu("ToggleRecording()")]
        public void ToggleRecording()
        {
            OBSBridge.Instance.ToggleRecording();
        }
    }
}
