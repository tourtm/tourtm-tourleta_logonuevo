﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using WebSocketSharp; // This library is available thanks to OBSWebsocketDotNet
using System.Security.Cryptography;
using Newtonsoft.Json.Linq;
using OBSWebsocketDotNet;
using UnityEngine.Events;


namespace App
{
    public partial class OBSBridge
    {
        private WebSocket ws;
        /// <summary>
        /// Invoked when obs open comand is executed
        /// </summary>
        public event UnityAction OnOBSOpenStart;
        /// <summary>
        /// Invoked when obs has correctly openeted and initialized its websocket server
        /// </summary>
        public event UnityAction OnOBSOpenCompleted;

        /// <summary>
        /// Creates a websocket client which will try to connect
        /// to the obs server, this will be used to detects when
        /// obs is ready.
        /// 
        /// I'm implementing this manual connection instead of calling obs.Connect() inside a try-catch
        /// to ensure if the obs web socket is already available because the OBSWebSocket library
        /// will block undefenetly if a request is send and server is not available yet. because of this line:
        /// https://github.com/Palakis/obs-websocket-dotnet/blob/076c6fd115d8bba8fae9ce7545565c2ecd8dc3ed/obs-websocket-dotnet/OBSWebsocket.cs#L320
        /// 
        /// Detailed explanation:
        /// Connect Method is called
        /// which calls Authenticate method
        /// which calls SendRequest("Authenticate", requestFields);
        /// which calls the TaskCompletionSource.Task.Wait() method
        /// which will block trhead until the TaskCompletionSource.Task.SetResult() gets called in WebsocketMessageHandler (that never happens, then blocking the thread forever)
        /// </summary>
        private void ServerAvailabilityInit()
        {
            // If websocket client already created, then don't do anything.
            if (ws != null) return;

            // The WebSocket class inherits the System.IDisposable
            // interface, so you can use the using statement.
            // And the WebSocket connection will be closed with
            // close status 1001 (going away) when the control
            // leaves the using block.
            // IMPORTANT NOTE: According to what i've read in the OBSWebSocket the 
            // websocket instance created on connection is not dispossed, or maybe is disposed in the disconnect
            // method. I will need to check.
            ws = new WebSocket(Url);

            // The OnWebsocketClose will be called when code exits the using statement
            ws.OnClose -= OnWebsocketClose;
            ws.OnClose += OnWebsocketClose;
        }

        /// <summary>
        /// This method is intended to be used as predicate in the WaitUntil class 
        /// in the unity coroutine system.
        /// </summary>
        /// <returns></returns>
        private bool isWebsocketServerAvailable()
        {
            if (ws == null)
            {
                Debug.Log("Testing connection client not created yet, try by calling ServerAvailabilityInit()");
            }

            // Try connection until the ReadyState is open
            Debug.Log("Trying connection to obs WebSocket server.");
            ws.Connect();
            Debug.Log("Is Connected: " + ws.ReadyState);
            return ws.ReadyState == WebSocketState.Open;
        }

        private void OnWebsocketClose(object sender, CloseEventArgs eventArgs)
        {
            Debug.Log("WebSocket client connected to check if server is availble has been correctly closed.");
        }

        private void DisposeServerAvailability()
        {
            if (ws == null) return;
            Debug.Log("DisposeServerAvailability()");
            // Closes the websocket connection, and releases all associated resources.
            ws.Close();
            ws = null;
        }
    }

}
