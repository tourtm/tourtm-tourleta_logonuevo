﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spinner : MonoBehaviour
{
    public AudioSource turnEffect;
    public float reducer;
    public float multiplier = 1.0f;
    bool round1 = false;
    public static bool isStoped = false;
    public MainController MController;
    public GameObject Needle;
    public Needle _needle;


    public bool canRotate = false;

    void Start()
    {
        turnEffect = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!canRotate) return;

        if (multiplier > 0.1f)
        {
            // this makes the rotation happen
            transform.Rotate(Vector3.forward, 1 * multiplier);
        }
        else if (!isStoped) // when rotation ends, opens a GUI menu
        {
            isStoped = true;
            canRotate = false;
            Needle.GetComponent<Needle>().SpinCheck();
            Debug.Log("Roullete has stoped...");
            turnEffect.Stop();
            //Debug.Log(multiplier);
        }

        if (multiplier < 20 && !round1)
        {
            multiplier += 0.1f;
        }
        else
        {
            round1 = true;
        }

        if (round1 && multiplier > 0)
        {
            multiplier -= reducer;
        }

    }


    public void Reset()
    {
        multiplier = 1;
        reducer = Random.Range(3f, 5f);
        round1 = false;
        isStoped = false;
        MController.OneTimeCall = true;
        MController.Challengetime = false;
        _needle.SpinPosition = 0; //******
        turnEffect.Stop();
    }


    public void MoveRoulete()
    {
        Reset();
        // multiplier = 1.0f;
        reducer = Random.Range(3f, 5f);
        canRotate = true;
        turnEffect.Play();
    }
}
