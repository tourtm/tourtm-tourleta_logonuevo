﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


[System.Serializable]
public struct SubtitleItem
{
    public float Second;
    public string Text;
    public Sprite Image;
}

public class SubtitleSystem : MonoBehaviour {

    public AudioSource song;
    // public Text subtitleText;
    public TextMeshProUGUI subtitleTextPro;
    public Image subtitleImage;
    public List<SubtitleItem> list;

    public float songTime;
    public float itemTime;
    public int currentIndex;

    // Use this for initialization
    void Start () {
    }

    private void OnEnable()
    {
        // subtitleText.gameObject.SetActive(true);
        // subtitleTextPro.gameObject.SetActive(true);
        subtitleImage.gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update () {
        int subtitleItem = 0;
        foreach (SubtitleItem item in list)
        {
            if (item.Second > song.time) break;
            subtitleItem++;
        }
        subtitleItem = Mathf.Clamp(subtitleItem, 0, list.Count - 1);

        SubtitleItem selectedItem = list[subtitleItem];
        string text = selectedItem.Text;
        // subtitleText.text = text;
        // subtitleTextPro.text = text;
        subtitleImage.sprite = selectedItem.Image;
        songTime = song.time;
        itemTime = selectedItem.Second;
        currentIndex = subtitleItem;

        // if is last and has ended
        if (subtitleItem == list.Count - 1 && selectedItem.Second < song.time)
        {
            // subtitleText.gameObject.SetActive(false);
            // subtitleTextPro.gameObject.SetActive(false);
            subtitleImage.gameObject.SetActive(false);
        }
    }
}
