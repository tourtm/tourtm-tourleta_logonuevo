﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App
{
    public class CameraViewer : Singleton<CameraViewer>
    {

        public WebCamTexture webcam = null;
        WebCamTexture webcamTexture = null;
        // Use this for initialization

        bool cameraBussy = false;

        void Awake()
        {
            if (WebCamTexture.devices.Length > 0)
            {
                webcamTexture = new WebCamTexture();
                Renderer renderer = GetComponent<Renderer>();
                webcam = webcamTexture;
                // webcamTexture.Play();
                Debug.LogFormat("<color=green>Camera Initialized</color>");
                // https://stackoverflow.com/questions/42952371/catch-all-exception-in-unity
                Application.logMessageReceived += LogCallback;
            }
            else
            {
                ShowCameraNotFoundWarning();
            }
        }

        // Update is called once per frame
        void Update()
        {

        }

        protected override void BeforeOnDestroy()
        {
            Application.logMessageReceived -= LogCallback;
        }

        // Called when there is an exception
        public void LogCallback(string condition, string stackTrace, LogType type)
        {
            string playBussyError = "Could not start graph";
            string pauseBussyError = "Could not pause pControl";
            if (type == LogType.Error)
            {
                if (condition == playBussyError || condition == pauseBussyError)
                {
                    Debug.Log(condition);
                    cameraBussy = true;
                    ShowCameraBussyWarning();
                }
            }
        }

        public void ShowCameraBussyWarning()
        {
            GameObject warningCamBussy = GameObject.FindGameObjectWithTag("warning_camera_bussy");
            Container container = warningCamBussy.GetComponent<Container>();
            container.ShowContent();
        }

        public void ShowCameraNotFoundWarning()
        {
            GameObject warningCamNotFound = GameObject.FindGameObjectWithTag("warning_camera_not_found");
            Container container = warningCamNotFound.GetComponent<Container>();
            container.ShowContent();
        }

        public void PlayCamera()
        {
            if (cameraBussy)
            {
                Debug.Log("CameraViewer::PlayCamera() cant be called while camera is being used by another app.");
                return;
            }
            CameraViewer.Instance.webcamTexture.Play();            
        }

        public void PauseCamera()
        {
            if (cameraBussy)
            {
                Debug.Log("CameraViewer::PauseCamera() cant be called while camera is being used by another app.");
                return;
            }
            CameraViewer.Instance.webcamTexture.Pause();
        }
    }


}

