﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class ButtonLazy : MonoBehaviour {

    public Button normalButton;
    public float keyLockSeconds = 0.3f;
    public float timeOfLastPress;
    public bool isLocked = false;

    public UnityEvent OnClick;


    // Use this for initialization
    void Start ()
    {
        // register to button on click
        normalButton.onClick.AddListener(OnNormalButtonClick);
    }
	
	// Update is called once per frame
	void Update ()
    {
        // is locked
        if (isLocked)
            isLocked = (Time.time - timeOfLastPress) < keyLockSeconds;
    }

    private void OnDestroy()
    {
        normalButton.onClick.RemoveListener(OnNormalButtonClick);
    }

    public void OnNormalButtonClick()
    {
        if (isLocked)
        {
            return;
        }
        else
        {
            timeOfLastPress = Time.time;
            isLocked = true;
        }
        OnClick.Invoke();
    }
}
