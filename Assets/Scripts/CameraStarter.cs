﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace App
{
    public class CameraStarter : MonoBehaviour
    {

        void Awake()
        {
            if (CameraViewer.Instance.webcam == null)
            {
                Debug.Log("Web cam is null");
            }
            StartCoroutine(AssignWebcam());
        }
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        IEnumerator AssignWebcam()
        {
            // Skips the current frame.
            yield return null;
            GetComponent<Renderer>().material.mainTexture = CameraViewer.Instance.webcam;
        }
    }

}
