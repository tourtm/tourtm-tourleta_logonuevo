﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;

namespace RockVR.Video.Demo
{
    public class PlayerButton : MonoBehaviour
    {
        public GameObject VideoPlTex;
        public List<GameObject> VideoPlayerUI;
        public AudioSource BgMusic;

        // Use this for initialization
        void Start()
        {}

        // Update is called once per frame
        void Update()
        {
            if(VideoCaptureCtrl.instance.status == VideoCaptureCtrl.StatusType.STOPPED)
            {
                //activates (.setactive) processing message
                VideoPlayerUI[5].SetActive(true);
                VideoPlayerUI[2].SetActive(false);

            }
            else if (VideoCaptureCtrl.instance.status == VideoCaptureCtrl.StatusType.FINISH)
            {
                VideoPlayerUI[5].SetActive(false);
                //activates the play video button
                VideoPlayerUI[4].SetActive(true);
                VideoPlayerUI[3].SetActive(true);
                VideoPlayerUI[2].SetActive(true);
            }
        }

        public void PlayTheVideo()
        {
            VideoPlayer.instance.SetRootFolder();
            // Play capture video.
            //VideoPlayer.instance.PlayVideo();
            string lastVideo = VideoPlayer.instance.videoFiles[VideoPlayer.instance.videoFiles.Count - 1];
            VideoPlayer.instance.PlayVideo(lastVideo); ///////custom line
            VideoPlTex.SetActive(true);

            BgMusic.Pause();
            
        }

    }
}
