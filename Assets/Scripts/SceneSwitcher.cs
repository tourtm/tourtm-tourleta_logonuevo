﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class SceneSwitcher : MonoBehaviour {


    public Spinner _spiner;

    public GameObject ChallengeManager;
    public GameObject Bar;
    public GameObject VideoSenderPlayer;
    public GameObject ChallengeTemplates;
    public GameObject RuletaBoton;
    public GameObject VideoTexture;
    public VideoPlayer VideoPL;
    

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    // Calledd when reset button gets pressed
    public void StartScene()
    {
        ChallengeManager.SetActive(false);
        Bar.SetActive(false);
        VideoSenderPlayer.SetActive(false);
        ChallengeTemplates.SetActive(false);
        RuletaBoton.SetActive(true);
        VideoTexture.SetActive(false);
        VideoPL.Stop();
    }

    public void ButtonRoulette()
    {
        RuletaBoton.SetActive(false);
    }

}
