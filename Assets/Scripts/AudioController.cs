﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

#if UNITYEDITOR
using UnityEditor;
#endif


public class AudioController : MonoBehaviour {

    public AudioMixerSnapshot MicActivated;
    public AudioMixerSnapshot MicDesactivated;
    public AudioMixerSnapshot Karaoke;


	// Use this for initialization
	void Start ()
    {
    }
	
	// Update is called once per frame
	void Update ()
    {
    }

    public void MicActive()
    {
        MicActivated.TransitionTo(0.3f);
    }

    public void MicDesactive()
    {
        MicDesactivated.TransitionTo(0.3f);
    }

    public void KaraokeSong()
    {
        Karaoke.TransitionTo(0.3f);
    }



}

