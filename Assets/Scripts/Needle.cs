﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Needle : MonoBehaviour
{

    public Spinner _spinner;
    public int SpinPosition = 0;
    public AudioSource audioSource;
    public AudioClip turnEffect;

    // Use this for initialization

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        // SpinCheck();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (Spinner.isStoped == false)
        {
            audioSource.PlayOneShot(turnEffect);
            Debug.Log("isStoped: " + Spinner.isStoped);
            // return;
        }
        if (Spinner.isStoped == true)
        {
            Debug.Log("isStoped: " + Spinner.isStoped);
        }
        if (col.gameObject.tag == "1") SpinPosition = 1;
        if (col.gameObject.tag == "2") SpinPosition = 2;
        if (col.gameObject.tag == "3") SpinPosition = 3;
        if (col.gameObject.tag == "4") SpinPosition = 4;
        if (col.gameObject.tag == "5") SpinPosition = 5;
        if (col.gameObject.tag == "6") SpinPosition = 6;
        if (col.gameObject.tag == "7") SpinPosition = 7;

    }

    public void SpinCheck()
    {
        Debug.Log(SpinPosition);
        Debug.Log("Slot " + SpinPosition);
    }


}

