﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TimeWaiter : MonoBehaviour
{

    public UnityEvent OnTimeEnd;
    public float secondsToWait = 3f;


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void StartTimer()
    {
        StartCoroutine(WaitFor(secondsToWait));
    }

    IEnumerator WaitFor(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        OnTimeEnd.Invoke();
    }
}