﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.IO;

namespace App
{
    public class GIFLoaderWindow : EditorWindow
    {

        readonly string emptyString = "";
        string[] frameFormat = new string[] { ".gif", ".jpg", ".png" };
        readonly string[] assetFolder = new string[] { "/Assets" };
        readonly string assetFolder2 = "Assets";

        [MenuItem("/Tools/Telemedellin/GIF Loader #%_l")]
        public static void Init()
        {
            // Get existing open window or if none, make a new one:
            GIFLoaderWindow gifLoader = (GIFLoaderWindow) EditorWindow.GetWindow(typeof(GIFLoaderWindow), false, "GIF loader", false);
            gifLoader.Show();
        }

        public void OnGUI()
        {
            EditorGUILayout.LabelField(new GUIContent("Loads an animated GIF from a folder"), EditorStyles.largeLabel);
            EditorGUILayout.LabelField(new GUIContent("Keep in mind that images should be imported as sprites."));
            GUILayout.Space(10);

            AnimatedGIF[] gifComponents = Selection.GetFiltered<AnimatedGIF>(SelectionMode.TopLevel);

            if (gifComponents.Length == 0)
            {
                EditorGUILayout.HelpBox("Select a game object with the AnimatedGIF script attached.", MessageType.Info);
                return;
            }
            
            if (GUILayout.Button("Load frames at folder"))
            {
                

                // The path in which are placed gif frames
                string path = EditorUtility.OpenFolderPanel("Select GIF frames folder", emptyString, emptyString);
                // If user cancels without selecting nothing, then, skips
                if (path == "") return;

                // The files in that directory
                string[] files = Directory.GetFiles(path);

                // Resets frames settled previously
                foreach (AnimatedGIF gif in gifComponents)
                {
                    gif.frames = new List<Sprite>();
                }

                int fileIndex = 0;
                foreach (string file in files)
                {
                    string[] fileParts = file.Split(frameFormat, System.StringSplitOptions.RemoveEmptyEntries);
                    // Skips if the file has extensions as .gif.meta
                    if (fileParts.Length > 1) continue;

                    // Loads frames
                    foreach (AnimatedGIF gif in gifComponents)
                    {
                        // The file path relative to asset folder
                        string relative2Asset = file.Split(assetFolder, System.StringSplitOptions.RemoveEmptyEntries)[1];
                        Sprite frame = AssetDatabase.LoadAssetAtPath<Sprite>(assetFolder2 + relative2Asset);
                        gif.frames.Add(frame);

                        // If is first frame, then assignt it to the Image to allow a basic preview
                        if (fileIndex == 0)
                        {
                            // Note that the Image component is required by the AnimatedGIF component
                            // and then is ensured the game object will have an instance of it.
                            Image image = gif.GetComponent<Image>();
                            image.sprite = frame;
                        }
                        
                    }
                    fileIndex++;
                }
            }
        }
    }
}
