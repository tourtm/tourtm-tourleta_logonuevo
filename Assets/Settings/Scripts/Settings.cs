﻿/*******************************************************
 * Copyright (C) 2017 Doron Weiss  - All Rights Reserved
 * You may use, distribute and modify this code under the
 * terms of unity license.
 * 
 * See https://abnormalcreativity.wixsite.com/home for more info
 *******************************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Dweiss {
    [System.Serializable]
    public class Settings : ASettings {
        [Header("OBS Installation Path")]
        // I'm not using @"c:\mypath\to\" operator because this will be loaded from a json file
        // and in the json file the @ outside of quotes is a syntax error.
        public string OBSExecutableFolder = "C:\\Program Files (x86)\\obs-studio\\bin\\64bit\\";
        public string OBSExecutableName = "obs64.exe";

        [Header("OBS Websocket Auth")]
        public string OBSWebsocketHost = "localhost";
        public int OBSWebsocketPort = 4444;
        public string OBSWebsocketPassword = "1234567890";

        [Space(10.0f)]
        [Header("Email SMTP info")]
        public string SMTPServer = "in-v3.mailjet.com";
        public int SMTPPort = 587;
        public string SMTPUserName = "96a1d530b2b00f6d500a721b2e10b805";
        public string SMTPUserPassword = "04b520b60a377265fe3aee7028f01be9";

        [Header("Email Message Options")]
        public string EmailMessageFrom = "tour@telemedellin.tv";
        // #userName# is a tag in the wrote email system.
        public string EmailMessageSubject = "Revive tu experiencia en la tourleta";

        [Header("Firebase Options")]
        public string FirebaseUploadPath = "gs://tourleta-b1f4a.appspot.com/tourleta/uploads/";


        private new void Awake() {
			base.Awake ();
            SetupSingelton();
        }


        #region  Singelton
        public static Settings _instance;
        public static Settings Instance { get { return _instance; } }
        private void SetupSingelton()
        {
            if (_instance != null)
            {
                Debug.LogError("Error in settings. Multiple singeltons exists: " + _instance.name + " and now " + this.name);
            }
            else
            {
                _instance = this;
            }
        }
        #endregion



    }
}